/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.difuture.components.i2b2transmart.configuration.Configuration;
import org.difuture.components.i2b2transmart.configuration.ConfigurationDatabase;
import org.difuture.components.util.LoggerUtil;

/**
 * Operations for i2b2
 * 
 * @author Fabian Prasser
 * @author Helmut Spengler
 *
 */
public class I2b2Operations {

    /** Config */
    private final Configuration config;

    /**
     * Creates a new class
     * 
     * @param config
     */
    public I2b2Operations(Configuration config) {
        super();
        this.config = config;
    }

    /**
     * Removes clinical data from DWH
     * 
     * @throws SQLException
     * @throws ClassNotFoundException 
     */
    public void delete() throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        Connection conn = DriverManager.getConnection(config.getDatabaseConfiguration().getUrl(),
                                                      config.getDatabaseConfiguration().getUser(),
                                                      config.getDatabaseConfiguration()
                                                            .getPassword());

        Statement stmt = conn.createStatement();

        LoggerUtil.info("Deleting all data from database", this.getClass());
        LoggerUtil.info(" - Deleting from table i2b2demodata.concept_dimension", this.getClass());
        String sql = "DELETE FROM i2b2demodata.concept_dimension WHERE sourcesystem_cd = 'TEST_SYS';";
        stmt.executeUpdate(sql);

        LoggerUtil.info(" - Deleting from table i2b2metadata.custom_meta", this.getClass());
        sql = "DELETE FROM i2b2metadata.custom_meta WHERE sourcesystem_cd = 'TEST_SYS';";
        stmt.executeUpdate(sql);

        LoggerUtil.info(" - Truncating table i2b2workdata.workplace_access", this.getClass());
        sql = "TRUNCATE TABLE i2b2workdata.workplace_access;";
        stmt.executeUpdate(sql);

        LoggerUtil.info(" - Truncating table i2b2metadata.birn", this.getClass());
        sql = "TRUNCATE i2b2metadata.birn;";
        stmt.executeUpdate(sql);

        LoggerUtil.info(" - Truncating table i2b2metadata.custom_meta", this.getClass());
        sql = "TRUNCATE i2b2metadata.custom_meta;";
        stmt.executeUpdate(sql);

        LoggerUtil.info(" - Truncating table i2b2metadata.schemes", this.getClass());
        sql = "TRUNCATE TABLE i2b2metadata.schemes;";
        stmt.executeUpdate(sql);

        LoggerUtil.info(" - Truncating table i2b2metadata.table_access", this.getClass());
        sql = "TRUNCATE TABLE i2b2metadata.table_access;";
        stmt.executeUpdate(sql);

        LoggerUtil.info(" - Truncating table i2b2metadata.i2b2", this.getClass());
        sql = "TRUNCATE TABLE i2b2metadata.i2b2;";
        stmt.executeUpdate(sql);

        LoggerUtil.info(" - Truncating table i2b2metadata.icd10_icd9", this.getClass());
        sql = "TRUNCATE TABLE i2b2metadata.icd10_icd9;";
        stmt.executeUpdate(sql);

        LoggerUtil.info(" - Truncating table i2b2demodata.concept_dimension", this.getClass());
        sql = "TRUNCATE TABLE i2b2demodata.concept_dimension;";
        stmt.executeUpdate(sql);

        LoggerUtil.info(" - Truncating table i2b2demodata.modifier_dimension", this.getClass());
        sql = "TRUNCATE TABLE i2b2demodata.modifier_dimension;";
        stmt.executeUpdate(sql);

        LoggerUtil.info(" - Truncating table i2b2demodata.encounter_mapping", this.getClass());
        sql = "TRUNCATE TABLE i2b2demodata.encounter_mapping;";
        stmt.executeUpdate(sql);

        LoggerUtil.info(" - Truncating table i2b2demodata.patient_dimension", this.getClass());
        sql = "TRUNCATE TABLE i2b2demodata.patient_dimension;";
        stmt.executeUpdate(sql);

        LoggerUtil.info(" - Truncating table i2b2demodata.patient_mapping", this.getClass());
        sql = "TRUNCATE TABLE i2b2demodata.patient_mapping;";
        stmt.executeUpdate(sql);

        LoggerUtil.info(" - Truncating table i2b2demodata.provider_dimension", this.getClass());
        sql = "TRUNCATE TABLE i2b2demodata.provider_dimension;";
        stmt.executeUpdate(sql);

        LoggerUtil.info(" - Truncating table i2b2demodata.qt_breakdown_path", this.getClass());
        sql = "TRUNCATE TABLE i2b2demodata.qt_breakdown_path;";
        stmt.executeUpdate(sql);

        LoggerUtil.info(" - Truncating table i2b2demodata.visit_dimension", this.getClass());
        sql = "TRUNCATE TABLE i2b2demodata.visit_dimension;";
        stmt.executeUpdate(sql);

        LoggerUtil.info(" - Truncating table i2b2demodata.observation_fact", this.getClass());
        sql = "TRUNCATE TABLE i2b2demodata.observation_fact;";
        stmt.executeUpdate(sql);

        LoggerUtil.info(" - Truncating table i2b2demodata.code_lookup", this.getClass());
        sql = "TRUNCATE TABLE i2b2demodata.code_lookup;";
        stmt.executeUpdate(sql);

        conn.close();
    }

    /**
     * Loads data into DWH using transmart-batch
     * 
     * @throws IOException
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public void load() throws IOException, ClassNotFoundException, SQLException {

        // Folder
        File folder = new File(config.getFolder().getAbsolutePath() + "/i2b2");

        // Check if we can login to db
        tryDbLogin(config.getDatabaseConfiguration());

        // Load ontologies
        File[] ontologies = folder.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File arg0, String arg1) {
                return arg1.endsWith("___ontology.xml");
            }
        });
        LoggerUtil.info("Found " + ontologies.length + " ontologies", this.getClass());
        Class.forName("org.postgresql.Driver");
        Connection conn = DriverManager.getConnection(config.getDatabaseConfiguration().getUrl(),
                                                      config.getDatabaseConfiguration().getUser(),
                                                      config.getDatabaseConfiguration()
                                                            .getPassword());
        I2b2OperationsOntology ontologyOperations = new I2b2OperationsOntology(conn,
                                                                               config.getSubjectConfiguration());
        for (File ontology : ontologies) {
            ontologyOperations.insert(ontology, config.getSourceSystemCode());
        }
        ontologyOperations.insertCodeLookup();
        conn.close();

        // Assemble command-line parameters
        String[] tmBatchParams = { "-p",
                                   folder + "/i2b2.params",
                                   "-n",
                                   "-c",
                                   folder + "/batchdb.properties" };

        LoggerUtil.info("Executing transmart-batch", this.getClass());

        // Execute transmart-batch
        org.transmartproject.batch.startup.RunJob.main(tmBatchParams);
    }

    /**
     * Prepares the database by creating the tables needed by Spring Batch.
     * Unfortunately, transmart-batch doesn't take care of this.
     * 
     * @throws SQLException
     * @throws ClassNotFoundException 
     */
    public void prepare() throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        Connection conn = DriverManager.getConnection(config.getDatabaseConfiguration().getUrl(),
                                                      config.getDatabaseConfiguration().getUser(),
                                                      config.getDatabaseConfiguration()
                                                            .getPassword());
        Statement stmt = conn.createStatement();

        // TODO: No need to drop and create, if it already exists

        LoggerUtil.info("Preparing database for execution of transmart batch", this.getClass());
        LoggerUtil.info(" - Dropping table TS_BATCH.BATCH_STEP_EXECUTION_CONTEXT", this.getClass());
        String sql = "DROP TABLE  IF EXISTS TS_BATCH.BATCH_STEP_EXECUTION_CONTEXT;";
        stmt.executeUpdate(sql);
        LoggerUtil.info(" - Dropping table TS_BATCH.BATCH_JOB_EXECUTION_CONTEXT", this.getClass());
        sql = "DROP TABLE  IF EXISTS TS_BATCH.BATCH_JOB_EXECUTION_CONTEXT;";
        stmt.executeUpdate(sql);
        LoggerUtil.info(" - Dropping table TS_BATCH.BATCH_STEP_EXECUTION", this.getClass());
        sql = "DROP TABLE  IF EXISTS TS_BATCH.BATCH_STEP_EXECUTION;";
        stmt.executeUpdate(sql);
        LoggerUtil.info(" - Dropping table TS_BATCH.BATCH_JOB_EXECUTION_PARAMS", this.getClass());
        sql = "DROP TABLE  IF EXISTS TS_BATCH.BATCH_JOB_EXECUTION_PARAMS;";
        stmt.executeUpdate(sql);
        LoggerUtil.info(" - Dropping table TS_BATCH.BATCH_JOB_EXECUTION", this.getClass());
        sql = "DROP TABLE  IF EXISTS TS_BATCH.BATCH_JOB_EXECUTION;";
        stmt.executeUpdate(sql);
        LoggerUtil.info(" - Dropping table TS_BATCH.BATCH_JOB_INSTANCE", this.getClass());
        sql = "DROP TABLE  IF EXISTS TS_BATCH.BATCH_JOB_INSTANCE;";
        stmt.executeUpdate(sql);

        LoggerUtil.info(" - Dropping sequence TS_BATCH.BATCH_STEP_EXECUTION_SEQ", this.getClass());
        sql = "DROP SEQUENCE  IF EXISTS TS_BATCH.BATCH_STEP_EXECUTION_SEQ ;";
        stmt.executeUpdate(sql);
        LoggerUtil.info(" - Dropping sequence TS_BATCH.BATCH_JOB_EXECUTION_SEQ", this.getClass());
        sql = "DROP SEQUENCE  IF EXISTS TS_BATCH.BATCH_JOB_EXECUTION_SEQ;";
        stmt.executeUpdate(sql);
        LoggerUtil.info(" - Dropping sequence TS_BATCH.BATCH_JOB_SEQ", this.getClass());
        sql = "DROP SEQUENCE  IF EXISTS TS_BATCH.BATCH_JOB_SEQ;";
        stmt.executeUpdate(sql);

        LoggerUtil.info(" - Dropping schema TS_BATCH", this.getClass());
        sql = "DROP SCHEMA IF EXISTS TS_BATCH;";
        stmt.executeUpdate(sql);

        LoggerUtil.info(" - Creating schema TS_BATCH", this.getClass());
        sql = "CREATE SCHEMA TS_BATCH;";
        stmt.executeUpdate(sql);

        LoggerUtil.info(" - Creating table TS_BATCH.BATCH_JOB_INSTANCE", this.getClass());
        sql = "CREATE TABLE TS_BATCH.BATCH_JOB_INSTANCE  (\r\n" +
              "    JOB_INSTANCE_ID BIGINT  NOT NULL PRIMARY KEY ,\r\n" +
              "    VERSION BIGINT ,\r\n" + "    JOB_NAME VARCHAR(100) NOT NULL,\r\n" +
              "    JOB_KEY VARCHAR(32) NOT NULL,\r\n" +
              "    constraint JOB_INST_UN unique (JOB_NAME, JOB_KEY)\r\n" + ");";
        stmt.executeUpdate(sql);
        LoggerUtil.info(" - Creating table TS_BATCH.BATCH_JOB_EXECUTION", this.getClass());
        sql = "CREATE TABLE TS_BATCH.BATCH_JOB_EXECUTION  (\r\n" +
              "    JOB_EXECUTION_ID BIGINT  NOT NULL PRIMARY KEY ,\r\n" +
              "    VERSION BIGINT  ,\r\n" + "    JOB_INSTANCE_ID BIGINT NOT NULL,\r\n" +
              "    CREATE_TIME TIMESTAMP NOT NULL,\r\n" +
              "    START_TIME TIMESTAMP DEFAULT NULL ,\r\n" +
              "    END_TIME TIMESTAMP DEFAULT NULL ,\r\n" + "    STATUS VARCHAR(10) ,\r\n" +
              "    EXIT_CODE VARCHAR(2500) ,\r\n" + "    EXIT_MESSAGE VARCHAR(2500) ,\r\n" +
              "    LAST_UPDATED TIMESTAMP,\r\n" +
              "    JOB_CONFIGURATION_LOCATION VARCHAR(2500) NULL,\r\n" +
              "    constraint JOB_INST_EXEC_FK foreign key (JOB_INSTANCE_ID)\r\n" +
              "    references TS_BATCH.BATCH_JOB_INSTANCE(JOB_INSTANCE_ID)\r\n" + ");";
        stmt.executeUpdate(sql);
        LoggerUtil.info(" - Creating table TS_BATCH.BATCH_JOB_EXECUTION_PARAMS", this.getClass());
        sql = "CREATE TABLE TS_BATCH.BATCH_JOB_EXECUTION_PARAMS  (\r\n" +
              "    JOB_EXECUTION_ID BIGINT NOT NULL ,\r\n" +
              "    TYPE_CD VARCHAR(6) NOT NULL ,\r\n" + "    KEY_NAME VARCHAR(100) NOT NULL ,\r\n" +
              "    STRING_VAL VARCHAR(250) ,\r\n" + "    DATE_VAL TIMESTAMP DEFAULT NULL ,\r\n" +
              "    LONG_VAL BIGINT ,\r\n" + "    DOUBLE_VAL DOUBLE PRECISION ,\r\n" +
              "    IDENTIFYING CHAR(1) NOT NULL ,\r\n" +
              "    constraint JOB_EXEC_PARAMS_FK foreign key (JOB_EXECUTION_ID)\r\n" +
              "    references TS_BATCH.BATCH_JOB_EXECUTION(JOB_EXECUTION_ID)\r\n" + ");";
        stmt.executeUpdate(sql);
        LoggerUtil.info(" - Creating table TS_BATCH.BATCH_STEP_EXECUTION", this.getClass());
        sql = "CREATE TABLE TS_BATCH.BATCH_STEP_EXECUTION  (\r\n" +
              "    STEP_EXECUTION_ID BIGINT  NOT NULL PRIMARY KEY ,\r\n" +
              "    VERSION BIGINT NOT NULL,\r\n" + "    STEP_NAME VARCHAR(100) NOT NULL,\r\n" +
              "    JOB_EXECUTION_ID BIGINT NOT NULL,\r\n" +
              "    START_TIME TIMESTAMP NOT NULL ,\r\n" +
              "    END_TIME TIMESTAMP DEFAULT NULL ,\r\n" + "    STATUS VARCHAR(10) ,\r\n" +
              "    COMMIT_COUNT BIGINT ,\r\n" + "    READ_COUNT BIGINT ,\r\n" +
              "    FILTER_COUNT BIGINT ,\r\n" + "    WRITE_COUNT BIGINT ,\r\n" +
              "    READ_SKIP_COUNT BIGINT ,\r\n" + "    WRITE_SKIP_COUNT BIGINT ,\r\n" +
              "    PROCESS_SKIP_COUNT BIGINT ,\r\n" + "    ROLLBACK_COUNT BIGINT ,\r\n" +
              "    EXIT_CODE VARCHAR(2500) ,\r\n" + "    EXIT_MESSAGE VARCHAR(2500) ,\r\n" +
              "    LAST_UPDATED TIMESTAMP,\r\n" +
              "    constraint JOB_EXEC_STEP_FK foreign key (JOB_EXECUTION_ID)\r\n" +
              "    references TS_BATCH.BATCH_JOB_EXECUTION(JOB_EXECUTION_ID)\r\n" + ");";
        stmt.executeUpdate(sql);
        LoggerUtil.info(" - Creating table TS_BATCH.BATCH_STEP_EXECUTION_CONTEXT", this.getClass());
        sql = "CREATE TABLE TS_BATCH.BATCH_STEP_EXECUTION_CONTEXT  (\r\n" +
              "    STEP_EXECUTION_ID BIGINT NOT NULL PRIMARY KEY,\r\n" +
              "    SHORT_CONTEXT VARCHAR(2500) NOT NULL,\r\n" +
              "    SERIALIZED_CONTEXT TEXT ,\r\n" +
              "    constraint STEP_EXEC_CTX_FK foreign key (STEP_EXECUTION_ID)\r\n" +
              "    references TS_BATCH.BATCH_STEP_EXECUTION(STEP_EXECUTION_ID)\r\n" + ");";
        stmt.executeUpdate(sql);
        LoggerUtil.info(" - Creating table TS_BATCH.BATCH_JOB_EXECUTION_CONTEXT", this.getClass());
        sql = "CREATE TABLE TS_BATCH.BATCH_JOB_EXECUTION_CONTEXT  (\r\n" +
              "    JOB_EXECUTION_ID BIGINT NOT NULL PRIMARY KEY,\r\n" +
              "    SHORT_CONTEXT VARCHAR(2500) NOT NULL,\r\n" +
              "    SERIALIZED_CONTEXT TEXT ,\r\n" +
              "    constraint JOB_EXEC_CTX_FK foreign key (JOB_EXECUTION_ID)\r\n" +
              "    references TS_BATCH.BATCH_JOB_EXECUTION(JOB_EXECUTION_ID)\r\n" + ");";
        stmt.executeUpdate(sql);

        LoggerUtil.info(" - Creating sequence TS_BATCH.BATCH_STEP_EXECUTION_SEQ", this.getClass());
        sql = "CREATE SEQUENCE TS_BATCH.BATCH_STEP_EXECUTION_SEQ MAXVALUE 9223372036854775807 NO CYCLE;";
        stmt.executeUpdate(sql);
        LoggerUtil.info(" - Creating sequence TS_BATCH.BATCH_JOB_EXECUTION_SEQ", this.getClass());
        sql = "CREATE SEQUENCE TS_BATCH.BATCH_JOB_EXECUTION_SEQ MAXVALUE 9223372036854775807 NO CYCLE;";
        stmt.executeUpdate(sql);
        LoggerUtil.info(" - Creating sequence TS_BATCH.BATCH_JOB_SEQ", this.getClass());
        sql = "CREATE SEQUENCE TS_BATCH.BATCH_JOB_SEQ MAXVALUE 9223372036854775807 NO CYCLE;";
        stmt.executeUpdate(sql);
        conn.close();
    }

    /**
     * Perform a test login on a database
     * 
     * @param database
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    private void tryDbLogin(ConfigurationDatabase database) throws SQLException,
                                                            ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        Connection connection = DriverManager.getConnection(database.getUrl(),
                                                            database.getUser(),
                                                            database.getPassword());
        connection.close();
    }
}
