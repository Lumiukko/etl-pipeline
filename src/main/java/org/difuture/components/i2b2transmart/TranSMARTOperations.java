/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart;

import java.io.File;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Types;

import org.difuture.components.i2b2transmart.configuration.Configuration;
import org.difuture.components.i2b2transmart.configuration.ConfigurationDatabase;
import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.util.LoggerUtil;

/**
 * Operations for transmart
 * 
 * @author Fabian Prasser
 * @author Helmut Spengler
 */
public class TranSMARTOperations {

    /** Config */
    private final Configuration config;

    /**
     * Creates a new class
     * 
     * @param config
     */
    public TranSMARTOperations(Configuration config) {
        this.config = config;
    }

    /**
     * Removes clinical data from DWH
     * 
     * @throws SQLException
     */
    public void delete() throws SQLException {
        LoggerUtil.info("Deleting study " + config.getStudyID() + " from database",
                        this.getClass());

        Connection conn = DriverManager.getConnection(config.getDatabaseConfiguration().getUrl(),
                                                      config.getDatabaseConfiguration().getUser(),
                                                      config.getDatabaseConfiguration()
                                                            .getPassword());

        // Connection conn =
        // DriverManager.getConnection(config.getDatabaseConfiguration().getUrl(),
        // "tm_dataloader", "tm_dataloader");

        CallableStatement backoutProc = conn.prepareCall("{ ? = call i2b2_backout_trial( ?, ?, ? ) }");
        backoutProc.registerOutParameter(1, Types.INTEGER);
        backoutProc.setString(2, config.getStudyID());
        backoutProc.setString(3, null);
        backoutProc.setString(4, null);
        backoutProc.execute();
        backoutProc.close();

        conn.close();

        LoggerUtil.info("- Study " + config.getStudyID() + " deleted from database",
                        this.getClass());
    }

    /**
     * Loads data into DWH using transmart-batch
     * 
     * @throws IOException
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public void load() throws IOException, ClassNotFoundException, SQLException {

        // TODO: Better
        if (Model.get().isUseTmDataLoader()) {
            loadTmDataLoader();
            return;
        }

        // Folder
        File folder = new File(config.getFolder().getAbsolutePath() + "/tranSMART");

        // Check if we can login to db
        tryDbLogin(config.getDatabaseConfiguration());

        // Assemble command-line parameters
        String[] tmBatchParams = { "-p",
                                   folder + "/clinical.params",
                                   "-n",
                                   "-c",
                                   folder + "/batchdb.properties" };

        LoggerUtil.info("Executing transmart-batch", this.getClass());

        // Execute transmart-batch
        org.transmartproject.batch.startup.RunJob.main(tmBatchParams);
    }

    /**
     * Loads data into TranSMART using tMDataLoader
     * 
     * @throws IOException
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public void loadTmDataLoader() throws IOException, ClassNotFoundException, SQLException {

        // Study ID
        String studyID = Model.get().getConfig().getStudyID();

        // Folder
        File folder = new File(config.getFolder().getAbsolutePath() + "/tranSMART/" + studyID);

        // Check if we can login to db
        tryDbLogin(config.getDatabaseConfiguration());

        // TODO: If true, use arg -visit-name-first
        // Model.get().getConfig().getVisitTop()

        // Assemble command-line parameters
        // TODO: In config.groovy may options be too
        String[] tmBatchParams = { "-check-duplicates", "-c", folder + "/Config.groovy" };

        LoggerUtil.info("Executing tMDataLoader", this.getClass());

        // Execute tMDataLoader
        com.thomsonreuters.lsps.transmart.etl.CommandLineTool.main(tmBatchParams);

        // Delete user
        TMDLSqlScripts.afterLoading();
    }

    /**
     * Prepares the database by running sql scripts required from tmDataLoader
     * 
     * @throws SQLException
     * @throws IOException
     */
    public void prepareForTmDataLoader() throws SQLException, IOException {
        TMDLSqlScripts.runSQLScripts();
    }

    /**
     * Perform a test login on a database
     * 
     * @param database
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    private void tryDbLogin(ConfigurationDatabase database) throws SQLException,
                                                            ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        Connection connection = DriverManager.getConnection(database.getUrl(),
                                                            database.getUser(),
                                                            database.getPassword());
        connection.close();
    }
}
