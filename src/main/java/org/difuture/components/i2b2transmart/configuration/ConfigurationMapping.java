/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.configuration;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

/**
 * The class ConfigurationMapping. Represents the mapping of a visit and its
 * subject as configured in configuration.properties.
 * 
 * @author Fabian Prasser
 */
public class ConfigurationMapping {

    /** File */
    public final File             file;
    /** Subject id */
    public final ConfigurationKey keySubject;
    /** Encounter id */
    public final ConfigurationKey keyEncounter;

    /**
     * Creates a new instance
     * 
     * @param file
     * @param subjectID
     * @param encounterID
     */
    public ConfigurationMapping(File file,
                                ConfigurationKey subjectID,
                                ConfigurationKey encounterID) {
        this.keySubject = subjectID;
        this.keyEncounter = encounterID;
        this.file = file;
    }

    /**
     * Gets the name of the attributes to read from the input file representing
     * subject and encounter.
     * 
     * @return a set
     */
    public Set<String> getSelectedColumns() {
        Set<String> result = new HashSet<>();
        result.addAll(keySubject.getKeyAttributes());
        result.addAll(keyEncounter.getKeyAttributes());
        return result;

    }
}
