/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.difuture.components.util.LoggerUtil;

/**
 * The class ColumnFilter.
 * 
 * Represents the configuration of the column filter.
 * 
 * It filters all data of a column having a configured value.
 * 
 * The column filter is optional and may be configured in
 * configuration.properties.
 * 
 * @author Claudia Lang
 *
 */
public class ConfigurationInputFileColumnFilter {

    /** The name of the attribute to apply the filter */
    private final List<String> columns;
    /** The value to filter */
    private final List<String> value;
    /** The indexes of the values to filter in the file */
    private List<Integer>      indexes;

    /**
     * Creates a new instance.
     * 
     * @param column
     *            - the name of the attribute to apply the filter.
     * @param value
     *            - the value to filter.
     */
    public ConfigurationInputFileColumnFilter(List<String> columns, List<String> value) {
        this.columns = columns;
        this.value = value;
    }

    /**
     * Gets a clone of the column filter object
     */
    @Override
    public ConfigurationInputFileColumnFilter clone() {
        return new ConfigurationInputFileColumnFilter(columns, value);
    }

    /**
     * Return columns
     * 
     * @return
     */
    public List<String> getColumns() {
        return columns;
    }

    /**
     * Return indexes
     * 
     * @return
     */
    public List<Integer> getIndexes() {
        return indexes;
    }

    /**
     * Applies the mapping
     * 
     * @param mappingColumns
     * @return
     */
    public ConfigurationInputFileColumnFilter map(Map<String, String> mappingColumns) {
        if (mappingColumns == null) { return this; }
        List<String> columns = new ArrayList<>();
        for (String column : this.columns) {
            columns.add(mappingColumns.getOrDefault(column, column));
        }
        return new ConfigurationInputFileColumnFilter(columns, this.value);
    }

    /**
     * Tests, whether the row passes the filter or not.
     * 
     * @param row
     *            - a string array, representing the row.
     * @return - a boolean (true, if the row passes the filter, false else)
     */
    public boolean passes(String[] row) {

        for (int i = 0; i < indexes.size(); i++) {
            if (!row[indexes.get(i)].equals(value.get(i))) { return false; }
        }
        return true;
    }

    /**
     * Initializes the list with the indexes of the columns.
     * 
     * @param header
     *            - a string array, representing the header.
     */
    public void prepare(String[] header) {

        // Prepare
        indexes = new ArrayList<>();

        // For each column
        for (String column : columns) {

            // Search
            boolean found = false;
            for (int i = 0; i < header.length; i++) {
                if (header[i].equals(column)) {
                    indexes.add(i);
                    found = true;
                    break;
                }
            }

            // Check
            if (!found) {
                LoggerUtil.fatalUnchecked("Column specified in filter not found: " + column,
                                          this.getClass(),
                                          IllegalArgumentException.class);
            }
        }
    }
}
