/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.difuture.components.i2b2transmart.ETL.Args;
import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.util.LoggerUtil;

/**
 * Configuration for the ETL process
 * 
 * @author Fabian Prasser
 * @author Helmut Spengler
 * @author Claudia Lang
 */
public class Configuration {

    /** Working folder */
    private File                             folder;
    /** Charset */
    private Charset                          charset;
    /** Study id */
    private String                           studyID;
    /** Security: Y or N */
    private boolean                          security;
    /** Files */
    private List<ConfigurationInputFile>     files          = new ArrayList<>();
    /** Subject */
    private ConfigurationSubject             subject;
    /** Mapping */
    private ConfigurationMapping             mapping;
    /** Encounter */
    private ConfigurationEncounter           encounter;
    /** Configuration database (i2b2 or transmart) */
    private ConfigurationDatabase            database;
    /** Configuration for pseudonymization */
    private ConfigurationPseudonymization    pseudonymization;
    /** ID for data source to be written into i2b2 schema */
    private String                           sourceSystemCode;
    /** ID for data source to be written into i2b2 schema. */
    private String                           provider;
    /** Use modifier as measure timestamp reliability - i2b2 specific */
    private boolean                          useMeasureTimestampReliability;
    /** The locale for which the decimal format is used */
    private String                           locale;
    /** The decimal Format */
    private String                           decimalFormat;
    /** At which point to load the visit */
    private boolean                          visitTop;
    /** Summary */
    private boolean                          summary;
    /** A list with VisitSelector objects */
    private List<ConfigurationVisitSelector> visitSelectors = new ArrayList<>();
    /** Command line args */
    private Args                             args;
    /** Tmdl use timestamp - tMDataLoader-specific */
    private Boolean                          tmdlUseTimestamp;

    /**
     * Creates a new instance
     * 
     * @param folder
     * @param ignoreFileData
     * @param args
     * @throws IOException
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public Configuration(File folder, boolean ignoreFileData, Args args) throws IOException,
                                                                         SQLException,
                                                                         ClassNotFoundException {

        if (!folder.isDirectory()) {
            LoggerUtil.fatalChecked("Specified path is not a directory",
                                    this.getClass(),
                                    IOException.class);
        }
        this.folder = folder;
        this.args = args;

        // Read
        read(new File(folder.getAbsolutePath()), ignoreFileData);

        // Checks
        checkConfigurationDatabase();
        checkPropertyPath();
    }

    /**
     * @return the charset
     */
    public Charset getCharset() {
        return charset;
    }

    /**
     * Gets the database configuration.
     * 
     * @return a ConfigurationDatabase object
     */
    public ConfigurationDatabase getDatabaseConfiguration() {
        return database;
    }

    /**
     * Gets the locale
     * 
     * @return
     */
    public String getDecimalFormat() {
        return decimalFormat;
    }

    /**
     * @return the encounter
     */
    public ConfigurationEncounter getEncounterConfiguration() {
        return encounter;
    }

    /**
     * @return the files
     */
    public List<ConfigurationInputFile> getFiles() {
        return files;
    }

    /**
     * @return the folder
     */
    public File getFolder() {
        return folder;
    }

    /**
     * Gets the locale
     * 
     * @return
     */
    public String getLocale() {
        return locale;
    }

    /**
     * @return the mapping
     */
    public ConfigurationMapping getMappingConfiguration() {
        return mapping;
    }

    /**
     * @return the provider needed for i2b2
     */
    public String getProvider() {
        return provider;
    }

    /**
     * Returns the PSN configuration
     * 
     * @return
     */
    public ConfigurationPseudonymization getPseudonymizationConfig() {
        return this.pseudonymization;
    }

    /**
     * @return the sourceSystemCode needed for i2b2
     */
    public String getSourceSystemCode() {
        return sourceSystemCode;
    }

    /**
     * @return the studyID needed for tranSMART
     */
    public String getStudyID() {
        return studyID;
    }

    /**
     * @return the subject
     */
    public ConfigurationSubject getSubjectConfiguration() {
        return subject;
    }

    /**
     * @return list with VisitSelector objects
     */
    public List<ConfigurationVisitSelector> getVisitSelectors() {
        return visitSelectors;
    }

    /**
     * Add visit at top
     * 
     * @return
     */
    public boolean getVisitTop() {
        return visitTop;
    }

    /**
     * @return the security
     */
    public boolean isSecurityRequired() {
        return security;
    }

    /**
     * Returns whether this is a summary database
     * 
     * @param summary
     *            - boolean
     */
    public boolean isSummary() {
        return this.summary;
    }

    /**
     * Gets whether to use timestamps (specific attribute for tMDataLoader)
     * 
     * Specific for tMDataLoader.
     * 
     * @return - null if property Timestamp is not configured - true if property
     *         Timestamp has value Y - false if property Timestamp has any other
     *         value but Y
     */
    public Boolean isTMDLUseTimestamp() {
        return tmdlUseTimestamp;
    }

    /**
     * Returns whether to use modifier to show measure timestamp reliability.
     * 
     * @return
     */
    public boolean isUseMeasureTimestampReliability() {
        return useMeasureTimestampReliability;
    }

    /**
     * Checks, whether the required properties to access the data base are
     * available. Throws an exception if not.
     */
    private void checkConfigurationDatabase() {
        // Check, whether the required properties to create the url are a
        // available
        if (database.getHost() == null || database.getPort() == null ||
            database.getInstance() == null) {
            LoggerUtil.fatalUnchecked("At least one property is missing to create url of the target system: host, port, instance.",
                                      Configuration.class,
                                      IllegalArgumentException.class);
        }

        // Check, whether login informations are available
        if (database.getUser() == null || database.getPassword() == null) {
            LoggerUtil.fatalUnchecked("No login information (user, password) of the target system available.",
                                      Configuration.class,
                                      IllegalArgumentException.class);
        }
    }

    /**
     * Performs some checks on the path property
     */
    private void checkPropertyPath() {
        Map<String, String> entityToPath = new HashMap<>();

        // Check, whether for subject.path no concatenation is configured
        if (subject.path.split("\\+").length > 1) {
            LoggerUtil.fatalUnchecked("Property Subjec.Path supports one folder only",
                                      Configuration.class,
                                      IllegalArgumentException.class);
        }

        // Check entities
        for (ConfigurationInputFile config : files) {
            String entityName = config.getName();
            String newPath = config.path;

            // Check, whether payload data and patient data use the same path
            if (subject.path.equals(newPath)) {
                LoggerUtil.fatalUnchecked("The value for Subject.Path must not be assigned to an entity",
                                          Configuration.class,
                                          IllegalArgumentException.class);
            }

            // Check, whether there are entities with the same name but with
            // different values for path
            String oldPath = entityToPath.get(entityName);
            if (oldPath == null) {
                entityToPath.put(entityName, newPath);
                continue;
            }
            if ((newPath == null && oldPath != null) || !newPath.equals(oldPath)) {
                String errorMessage = "There are entities having the same entity name (" +
                                      entityName + ") but not the same value for property .Path";
                LoggerUtil.fatalUnchecked(errorMessage,
                                          Configuration.class,
                                          IllegalArgumentException.class);
            }
        }
    }

    /**
     * Reads concatenated values for properties and returns them in a list.
     * 
     * @param entry
     * @return
     */
    private List<String> getConcatenationAsString(String entry) {
        String[] fields = entry.split("\\+");
        return Arrays.asList(fields);
    }

    /**
     * Reads a filename
     * 
     * @param filename
     * @return
     * @throws IOException
     */
    private File getFile(String filename) throws IOException {
        File file = new File(folder.getAbsoluteFile() + "/" + filename);
        if (!file.exists()) {
            LoggerUtil.fatalChecked("File does not exist: " + filename,
                                    this.getClass(),
                                    IOException.class);
        }
        return file;
    }

    /**
     * Gets an optional field
     * 
     * @param properties
     * @param field
     */
    private String getOptional(Properties properties, String field) {
        String value = properties.getProperty(field);
        if (value != null) {
            value = value.trim();
        }
        return value;
    }

    /**
     * Gets a required field
     * 
     * @param properties
     * @param field
     */
    private String getRequired(Properties properties, String field) {
        String value = properties.getProperty(field);
        if (value != null) {
            value = value.trim();
        } else {
            LoggerUtil.fatalUnchecked("Property must be specified: " + field,
                                      this.getClass(),
                                      IllegalArgumentException.class);
        }
        return value;
    }

    /**
     * Reads the properties
     * 
     * @param path
     * @param ignoreFileData
     * @throws IOException
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    private void read(File path, boolean ignoreFileData) throws IOException,
                                                         SQLException,
                                                         ClassNotFoundException {

        // Find relevant files
        File[] files = path.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                if (!name.endsWith(".properties")) { return false; }
                if (Model.get().isI2b2() && name.endsWith("_tm.properties")) {
                    LoggerUtil.info("Target is i2b2. Skipping configuration file " + name,
                                    this.getClass());
                    return false;
                }
                if (!Model.get().isI2b2() && name.endsWith("_i2b2.properties")) {
                    LoggerUtil.info("Target is tranSMART. Skipping configuration file " + name,
                                    this.getClass());
                    return false;
                }
                return true;
            }
        });

        // Load properties files
        Properties properties = new Properties();
        for (File file : files) {
            FileInputStream stream = new FileInputStream(file);
            properties.load(stream);
            stream.close();
        }

        // Parse configuration
        readPseudonymizationConfiguration(properties);
        readBasicConfiguration(properties);
        readTmDataLoaderProperties(properties);
        if (!ignoreFileData) {
            readInputFiles(properties);
            readSubject(properties);
            if (getOptional(properties, "Encounter.File") != null &&
                getOptional(properties, "Mapping.File") != null) {
                readEncounter(properties);
                readMapping(properties);
            }
        }
        readDatabases(properties);
        readVisitSelectors(properties);
    }

    /**
     * Reads basic configuration
     * 
     * @param properties
     */
    private void readBasicConfiguration(Properties properties) {

        String charset;
        if (Model.get().isI2b2()) {

            // Charset
            charset = getOptional(properties, "I2b2.Charset");

            // Read ID for data source
            this.sourceSystemCode = getRequired(properties, "I2b2.Sourcesystem.Code");

            // Provider
            this.provider = getRequired(properties, "I2b2.Provider");

        } else {

            // Charset
            charset = getOptional(properties, "TranSMART.Charset");

            // Read study ID
            this.studyID = getRequired(properties, "TranSMART.StudyID");

            // Read security
            String security = getRequired(properties, "TranSMART.Security");
            if (!(security.equals("Y") || security.equals("N"))) {
                LoggerUtil.fatalUnchecked("Security must be specified as Y/N",
                                          this.getClass(),
                                          IllegalArgumentException.class);
            }

            // Summary
            String _summary = getOptional(properties, "TranSMART.Summary");
            this.summary = (_summary != null && _summary.equals("Y"));

        }

        // Charset
        if ("default".equals(charset)) {
            this.charset = Charset.defaultCharset();
        } else if (charset != null) {
            this.charset = Charset.forName(charset);
        }

        // Locale
        this.locale = getOptional(properties, "Number.Locale");

        // Decimal format
        this.decimalFormat = getOptional(properties, "Number.Format");

        // Display visit at top
        String _top = getOptional(properties, "Visit.Top");
        this.visitTop = (_top == null || _top.equals("Y"));

        // Use measure for timestamp reliability
        String _useMeasureTimestampReliability = getOptional(properties,
                                                             "MeasureTimestampReliability");
        this.useMeasureTimestampReliability = _useMeasureTimestampReliability == null ||
                                              !_useMeasureTimestampReliability.equals("Y") ? false
                                                      : true;
    }

    /**
     * Reads a column filter for an entity
     * 
     * @param name
     * @param properties
     * @return
     */
    private ConfigurationInputFileColumnFilter[] readColumnFilter(String name,
                                                                  Properties properties) {

        // TODO: Implement support for specifying multiple filters
        String _columns = getOptional(properties, name + ".Filter.Column");
        if (_columns == null) { return null; }
        List<String> columns = getConcatenationAsString(_columns);
        String value = getRequired(properties, name + ".Filter.Value");
        return new ConfigurationInputFileColumnFilter[] { new ConfigurationInputFileColumnFilter(columns,
                                                                                                 getConcatenationAsString(value)) };
    }

    /**
     * Reads database parameters
     * 
     * @param properties
     * @param dbTarget
     * @throws IOException
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    private void readDatabases(Properties properties) throws IOException,
                                                      SQLException,
                                                      ClassNotFoundException {

        // Prefix
        String prefixTarget = (Model.get().isI2b2()) ? "I2b2" : "TranSMART";

        // If data base properties were not available at command arguments, read
        // them now
        String host = getOptional(properties, prefixTarget + ".Database.Host");
        String port = getOptional(properties, prefixTarget + ".Database.Port");
        String instance = getOptional(properties, prefixTarget + ".Database.Instance");
        String user = getOptional(properties, prefixTarget + ".Database.User");
        String password = getOptional(properties, prefixTarget + ".Database.Password");

        // Read from command line
        host = args.host == null ? host : args.host;
        port = args.port == null ? port : args.port;
        instance = args.instance == null ? instance : args.instance;
        user = args.user == null ? user : args.user;
        password = args.password == null ? password : args.password;

        // Check
        if (host == null) {
            LoggerUtil.fatalUnchecked("To database host specified",
                                      this.getClass(),
                                      IllegalArgumentException.class);
        }
        if (port == null) {
            LoggerUtil.fatalUnchecked("To database port specified",
                                      this.getClass(),
                                      IllegalArgumentException.class);
        }
        if (instance == null) {
            LoggerUtil.fatalUnchecked("To database instance specified",
                                      this.getClass(),
                                      IllegalArgumentException.class);
        }

        // Create
        this.database = new ConfigurationDatabase(host, port, instance, user, password);
    }

    /**
     * Reads an EAV configuration for an entity
     * 
     * @param name
     * @param properties
     * @param attribute
     * @return
     */
    private ConfigurationInputFileEAV
            readEAV(String name, Properties properties, String attribute) {
        String _entity = getOptional(properties, name + ".EAV.Entity");
        if (_entity == null) { return null; }
        List<String> entity = getConcatenationAsString(_entity);
        String value = getOptional(properties, name + ".EAV.Value");
        String explicit = getOptional(properties, name + ".EAV.Explicit");
        return new ConfigurationInputFileEAV(entity,
                                             value,
                                             attribute,
                                             explicit != null && explicit.equals("Y"));
    }

    /**
     * Reads encounter configuration
     * 
     * @param properties
     * @throws IOException
     */
    private void readEncounter(Properties properties) throws IOException {
        File file = getFile(getRequired(properties, "Encounter.File"));
        String source = getOptional(properties, "Encounter.Source");
        String use = getOptional(properties, "Encounter.Use");
        ConfigurationKey key = new ConfigurationKey(getConcatenationAsString(getRequired(properties,
                                                                                         "Encounter.Encounter")),
                                                    source,
                                                    use);
        String visitID = getOptional(properties, "Encounter.VisitID");
        String start = getOptional(properties, "Encounter.Start");
        String end = getOptional(properties, "Encounter.End");
        if (visitID == null) {
            start = getRequired(properties, "Encounter.Start");
            end = getRequired(properties, "Encounter.End");
        }
        String activeStatus = getOptional(properties, "Encounter.ActiveStatus");
        String inout = getOptional(properties, "Encounter.Inout");
        String location = getOptional(properties, "Encounter.Location");
        String locationPath = getOptional(properties, "Encounter.LocationPath");
        String lengthOfStay = getOptional(properties, "Encounter.LengthOfStay");
        String blob = getOptional(properties, "Encounter.Blob");
        String dateFormat = getOptional(properties, "Encounter.DateFormat");
        this.encounter = new ConfigurationEncounter(file,
                                                    key,
                                                    start,
                                                    end,
                                                    visitID,
                                                    dateFormat,
                                                    activeStatus,
                                                    inout,
                                                    location,
                                                    locationPath,
                                                    lengthOfStay,
                                                    blob);
    }

    /**
     * Reads one input file
     * 
     * @param name
     * @throws IOException
     */
    private ConfigurationInputFile readInputFile(Properties properties,
                                                 String _name) throws IOException {

        // Prepare
        Set<String> whitelist = new HashSet<>();
        Set<String> blacklist = new HashSet<>();
        Set<File> files = new HashSet<>();
        Map<String, String> mappingColumns = new HashMap<>();
        String entityLocale = null;
        String entityDecimalFormat = null;

        // Read
        Map<String, ConfigurationValueMapping> valueMappings = new HashMap<>();
        files.add(getFile(getRequired(properties, _name)));
        String name = getRequired(properties, _name + ".Name");
        String _encounter = getOptional(properties, _name + ".Encounter");
        String _subject = getOptional(properties, _name + ".Subject");
        for (String key : properties.stringPropertyNames()) {

            if (key.startsWith(_name + ".Append.")) {
                files.add(getFile(properties.getProperty(key)));
            } else if (key.startsWith(_name + ".Blacklist.")) {
                blacklist.add(properties.getProperty(key));
            } else if (key.startsWith(_name + ".Whitelist.")) {
                whitelist.add(properties.getProperty(key));
            } else if (key.startsWith(_name + ".Mapping.")) {
                mappingColumns.put(key.replace(_name + ".Mapping.", ""),
                                   properties.getProperty(key));
            } else if (key.equals(_name + ".Number.Locale")) {
                entityLocale = getOptional(properties, _name + ".Number.Locale");
            } else if (key.equals(_name + ".Number.Format")) {
                entityDecimalFormat = getOptional(properties, _name + ".Number.Format");
            } else if (key.startsWith(_name + ".ValueMapping.") && key.endsWith(".Attribute")) {
                // Extract value mapping
                Map<String, String> mapping = new HashMap<>();
                Map<String, String> wildcard = new HashMap<>();
                String _default = null;
                String attribute = getRequired(properties, key);
                String prefix = key.substring(0, key.length() - "Attribute".length());
                for (String key2 : properties.stringPropertyNames()) {
                    if (key2.startsWith(prefix)) {

                        if (key2.endsWith("NonMatch")) {

                            // Default
                            _default = getRequired(properties, key2);
                        } else if (key2.contains(".Wildcard")) {

                            // Wildcard mapping
                            String value = key2.replace(".Wildcard", "").substring(prefix.length());
                            String mappedValue = getRequired(properties, key2);
                            wildcard.put(value, mappedValue);
                        } else {
                            // Standard mapping
                            String value = key2.substring(prefix.length());
                            String mappedValue = getRequired(properties, key2);
                            mapping.put(value, mappedValue);
                        }
                    }
                }
                valueMappings.put(attribute,
                                  new ConfigurationValueMapping(attribute,
                                                                mapping,
                                                                wildcard,
                                                                _default));
            }
        }

        // Check
        if (!blacklist.isEmpty() && !whitelist.isEmpty()) {
            LoggerUtil.fatalUnchecked(_name + " has blacklist and whitelist",
                                      this.getClass(),
                                      IllegalArgumentException.class);
        }

        // Read key
        ConfigurationKey keyID = null;
        boolean hasEncounter = false;
        boolean matchOneToMany = _encounter != null &&
                                 _encounter.equals("MatchByTimestampOneToMany");
        if (_encounter != null && !_encounter.equals("MatchByTimestamp") &&
            !_encounter.equals("MatchByTimestampOneToMany")) {
            keyID = new ConfigurationKey(getConcatenationAsString(_encounter));
            hasEncounter = true;
        } else if (_subject != null) {
            keyID = new ConfigurationKey(getConcatenationAsString(_subject));
        } else {
            LoggerUtil.fatalUnchecked(_name + " has no association to subject or encounter",
                                      this.getClass(),
                                      IllegalArgumentException.class);
        }

        // Timestamps
        String start = getOptional(properties, _name + ".Start");
        String end = getOptional(properties, _name + ".End");

        // Start: day, month, year
        String startDay = getOptional(properties, _name + ".Start.Day");
        String startMonth = (startDay == null) ? getOptional(properties, _name + ".Start.Month")
                : getRequired(properties, _name + ".Start.Month");
        String startYear = (startMonth == null) ? getOptional(properties, _name + ".Start.Year")
                : getRequired(properties, _name + ".Start.Year");

        // End: day, month, year
        String endDay = getOptional(properties, _name + ".End.Day");
        String endMonth = (endDay == null) ? getOptional(properties, _name + ".End.Month")
                : getRequired(properties, _name + ".End.Month");
        String endYear = (endMonth == null) ? getOptional(properties, _name + ".End.Year")
                : getRequired(properties, _name + ".End.Year");

        // Checks
        if (startYear != null && startMonth == null) {
            String errorMsg = "Property Start.Year specified for " + _name +
                              " but Start.Month is not specified (Start.Day is optional)!";
            LoggerUtil.fatalUnchecked(errorMsg, this.getClass(), IllegalArgumentException.class);
        }
        if (endYear != null && endMonth == null) {
            String errorMsg = "Property End.Year specified for " + _name +
                              " but End.Month is not specified (End.Day is optional)!";
            LoggerUtil.fatalUnchecked(errorMsg, this.getClass(), IllegalArgumentException.class);
        }
        if (start != null && startYear != null) {
            String errorMsg = "Too many properties regarding the start date are specified for " +
                              _name + "!";
            LoggerUtil.fatalUnchecked(errorMsg, this.getClass(), IllegalArgumentException.class);
        }
        if (end != null && endYear != null) {
            String errorMsg = "Too many properties regarding the end date are specified for " +
                              _name + "!";
            LoggerUtil.fatalUnchecked(errorMsg, this.getClass(), IllegalArgumentException.class);
        }

        // Date
        String dateFormat = getOptional(properties, _name + ".DateFormat");

        // Path
        String path = getOptional(properties, _name + ".Path");
        // For i2b2 a path with a length > 1 is not supported.
        if (Model.get().isI2b2() && path != null && path.split("\\+").length > 1) {
            path = path.replaceAll("\\+", "/");
            LoggerUtil.warn("For i2b2 a path with a length > 1 is not supported, so path for entity '" +
                            name + "' was changed to: " + path,
                            Configuration.class);
        }

        // Concept
        String concept = getOptional(properties, _name + ".Concept");

        // Charset
        String _charset = getOptional(properties, _name + ".Charset");
        if ("default".equals(_charset)) {
            charset = Charset.defaultCharset();
        } else if (_charset != null) {
            charset = Charset.forName(_charset);
        }

        // Use measure for reliability of timestamp
        String _useMeasureTimestampReliability = getOptional(properties,
                                                             _name + ".MeasureTimestampReliability");
        boolean useMeasureTimestampReliability = this.useMeasureTimestampReliability;
        if (_useMeasureTimestampReliability != null) {
            if (_useMeasureTimestampReliability.equals("Y")) {
                useMeasureTimestampReliability = true;
            } else {
                useMeasureTimestampReliability = false;
            }
        }

        // Is timestamp to use?
        boolean useTimestamp;
        String _useTimestamp = getOptional(properties, _name + ".TMDL.UseTimestamp");
        if (_useTimestamp != null) {
            if (_useTimestamp.equals("Y")) {
                useTimestamp = true;
            } else {
                useTimestamp = false;
            }
        } else {
            useTimestamp = this.tmdlUseTimestamp != null && this.tmdlUseTimestamp;
        }

        // No summary
        boolean noSummary = "Y".equals(getOptional(properties, _name + ".NoSummary"));

        // Return entity
        return new ConfigurationInputFile(files,
                                          keyID,
                                          name,
                                          mappingColumns,
                                          whitelist,
                                          blacklist,
                                          hasEncounter,
                                          start,
                                          end,
                                          dateFormat,
                                          concept,
                                          entityLocale == null ? getLocale() : entityLocale,
                                          entityDecimalFormat == null ? getDecimalFormat()
                                                  : entityDecimalFormat,
                                          path,
                                          startDay,
                                          startMonth,
                                          startYear,
                                          endDay,
                                          endMonth,
                                          endYear,
                                          _encounter != null &&
                                                   (_encounter.equals("MatchByTimestamp") ||
                                                    _encounter.equals("MatchByTimestampOneToMany")),
                                          matchOneToMany,
                                          charset,
                                          readColumnFilter(_name, properties),
                                          readEAV(_name, properties, name),
                                          useMeasureTimestampReliability,
                                          valueMappings,
                                          useTimestamp,
                                          noSummary);
    }

    /**
     * Read input files
     * 
     * @param properties
     * @throws IOException
     */
    private void readInputFiles(Properties properties) throws IOException {
        List<String> files = new ArrayList<String>();
        for (Object key : properties.keySet()) {
            String string = String.valueOf(key);
            if (string.startsWith("File") && !string.contains(".")) {
                files.add(string);
            }
        }
        for (String file : files) {
            this.files.add(readInputFile(properties, file));
        }
    }

    /**
     * Reads mapping configuration
     * 
     * @param properties
     * @throws IOException
     */
    private void readMapping(Properties properties) throws IOException {
        File file = getFile(getRequired(properties, "Mapping.File"));
        ConfigurationKey encounter = new ConfigurationKey(getConcatenationAsString(getRequired(properties,
                                                                                               "Mapping.Encounter")));
        ConfigurationKey subject = new ConfigurationKey(getConcatenationAsString(getRequired(properties,
                                                                                             "Mapping.Subject")));
        this.mapping = new ConfigurationMapping(file, subject, encounter);
    }

    /**
     * Reads pseudonymization configuration
     * 
     * @param properties
     */
    private void readPseudonymizationConfiguration(Properties properties) {

        // Check if enabled
        String enabled = getOptional(properties, "Pseudonymization.Enabled");
        if (!"Y".equals(enabled)) { return; }

        // Parse parameters
        String psnURL = getRequired(properties, "Pseudonymization.PsnURL");
        String authURL = getRequired(properties, "Pseudonymization.AuthURL");
        String authClient = getRequired(properties, "Pseudonymization.AuthClient");
        String authClientSecret = getOptional(properties, "Pseudonymization.AuthClientSecret");

        // Read from command line
        authClientSecret = args.clientSecret == null ? authClientSecret : args.clientSecret;

        // Check
        if (authClientSecret == null) {
            LoggerUtil.fatalUnchecked("No client secret specified for accessing the pseudonymization service",
                                      this.getClass(),
                                      IllegalArgumentException.class);
        }

        // Store
        this.pseudonymization = new ConfigurationPseudonymization(psnURL,
                                                                  authURL,
                                                                  authClient,
                                                                  authClientSecret);
    }

    /**
     * Reads subject configuration
     * 
     * @param properties
     * @throws IOException
     */
    private void readSubject(Properties properties) throws IOException {

        // Read stuff
        File file = getFile(getRequired(properties, "Subject.File"));
        String source = getOptional(properties, "Subject.Source");
        String use = getOptional(properties, "Subject.Use");
        ConfigurationKey key = new ConfigurationKey(getConcatenationAsString(getRequired(properties,
                                                                                         "Subject.Subject")),
                                                    source,
                                                    use);
        String sex = getOptional(properties, "Subject.Sex");
        String zip = getOptional(properties, "Subject.ZIP");
        String age = getOptional(properties, "Subject.Age");
        String birthYear = getOptional(properties, "Subject.BirthYear");
        String birthMonth = getOptional(properties, "Subject.BirthMonth");
        String birthDate = getOptional(properties, "Subject.BirthDate");
        String dateFormat = getOptional(properties, "Subject.DateFormat");
        String sexFemale = getOptional(properties, "Subject.Sex.Female");
        String sexMale = getOptional(properties, "Subject.Sex.Male");
        String sexUnknown = getOptional(properties, "Subject.Sex.Unknown");
        String vitalStatus = getOptional(properties, "Subject.VitalStatus");
        String language = getOptional(properties, "Subject.Language");
        String race = getOptional(properties, "Subject.Race");
        String maritalStatus = getOptional(properties, "Subject.MaritalStatus");
        String religion = getOptional(properties, "Subject.Religion");
        String statecityzipPath = getOptional(properties, "Subject.StatecityzipPath");
        String income = getOptional(properties, "Subject.Income");
        String blob = getOptional(properties, "Subject.Blob");
        String path = getOptional(properties, "Subject.Path");

        path = path == null ? "Demographics" : path;

        String _keepAll = getOptional(properties, "Subject.KeepAll");
        boolean keepAll = _keepAll == null || !_keepAll.equals("Y") ? false : true;

        // Load ids
        boolean loadIDs = "Y".equals(getOptional(properties, "Subject.LoadID"));

        // Plausibility tests
        if (age != null && age.equals("CALCULATE")) {
            // If the age is to calculate, it is necessary that an encounter,
            // a mapping and an attribute with the birth date are configured
            getRequired(properties, "Encounter.File");
            getRequired(properties, "Mapping.File");
            getRequired(properties, "Subject.BirthDate");
        }

        // Create object
        this.subject = new ConfigurationSubject(file,
                                                key,
                                                dateFormat,
                                                keepAll,
                                                sex,
                                                zip,
                                                age,
                                                birthYear,
                                                birthMonth,
                                                birthDate,
                                                sexFemale,
                                                sexMale,
                                                sexUnknown,
                                                vitalStatus,
                                                language,
                                                race,
                                                maritalStatus,
                                                religion,
                                                statecityzipPath,
                                                income,
                                                blob,
                                                path,
                                                loadIDs);
    }

    /**
     * Reads the properties specific for tMDataLoader:
     * 
     * - timestamp: Required for Advance Workflow Line Graph for numerical
     * values.
     * 
     * @param properties
     * @throws IOException
     */
    private void readTmDataLoaderProperties(Properties properties) throws IOException {
        if (!Model.get().isUseTmDataLoader()) { return; }

        // Is timestamp to use?
        String _useTimestamp = getOptional(properties, "TranSMART.TMDL.UseTimestamp");
        if (_useTimestamp != null) {
            if (_useTimestamp.equals("Y")) {
                this.tmdlUseTimestamp = true;
            } else {
                this.tmdlUseTimestamp = false;
            }
        }
    }

    /**
     * Read properties for VisitSelector objects
     * 
     * @param properties
     * @throws IOException
     */
    private void readVisitSelectors(Properties properties) throws IOException {

        // Are all visits required or are gaps allowed?
        String _allVisitsRequired = getOptional(properties, "Visit.Select.AllVisitsRequired");
        boolean allVisitsRequired = _allVisitsRequired == null || !_allVisitsRequired.equals("Y")
                ? false
                : true;
        ConfigurationVisitSelector.ALL_VISITS_REQUIRED = allVisitsRequired;

        // Should visits in the same timeframe be merged?
        String _mergeVisits = getOptional(properties, "Visit.Select.Merge");
        boolean mergeVisits = _mergeVisits == null || !_mergeVisits.equals("Y") ? false : true;
        ConfigurationVisitSelector.MERGE = mergeVisits;

        // For each key
        for (Object key : properties.keySet()) {
            String string = String.valueOf(key);
            if (string.startsWith("Visit.Select.") && string.contains("Month")) {
                String prefix = string.substring(0, string.lastIndexOf("."));
                int startMonth = Integer.parseInt(getRequired(properties, prefix + ".StartMonth"));
                int endMonth = Integer.parseInt(getRequired(properties, prefix + ".EndMonth"));
                String name = getRequired(properties, prefix + ".Name");
                ConfigurationVisitSelector selector = new ConfigurationVisitSelector(startMonth,
                                                                                     endMonth,
                                                                                     name);
                visitSelectors.add(selector);
            }
        }
    }
}
