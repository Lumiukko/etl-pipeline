/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.configuration;

/**
 * The class ConfigurationDatabase. Represents the settings for the database as
 * configured in configuration.properties.
 * 
 * @author Helmut Spengler
 * @author Claudia Lang
 *
 */
public class ConfigurationDatabase {

    /** User */
    private String user;
    /** Password */
    private String password;
    /** Host */
    private String host;
    /** Port */
    private String port;
    /** Instance */
    private String instance;

    /**
     * Creates a new instance
     *
     * @param host
     * @param port
     * @param instance
     * @param user
     * @param password
     */
    public ConfigurationDatabase(String host,
                                 String port,
                                 String instance,
                                 String user,
                                 String password) {
        this.host = host;
        this.port = port;
        this.instance = instance;
        this.user = user;
        this.password = password;
    }

    /**
     * Gets the host name
     * 
     * @return a string
     */
    public String getHost() {
        return host;
    }

    /**
     * Gets the name of the instance
     * 
     * @return a string
     */
    public String getInstance() {
        return instance;
    }

    /**
     * Gets the password
     * 
     * @return a string
     */
    public String getPassword() {
        return password;
    }

    /**
     * Gets the port
     * 
     * @return a string
     */
    public String getPort() {
        return port;
    }

    /**
     * Gets the url
     * 
     * @return a string
     */
    public String getUrl() {
        return "jdbc:postgresql://" + host + ":" + port + "/" + instance;
    }

    /**
     * Gets the name of the user of the data base
     * 
     * @return a string
     */
    public String getUser() {
        return user;
    }

    /**
     * Sets the name of the host
     * 
     * @param host
     *            - a string
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * Sets the name of the instance
     * 
     * @param instance
     *            - a string
     */
    public void setInstance(String instance) {
        this.instance = instance;
    }

    /**
     * Sets the password
     * 
     * @param password
     *            - string
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Sets the port
     * 
     * @param port
     *            - a string
     */
    public void setPort(String port) {
        this.port = port;
    }

    /**
     * Sets the name of the user of the data base
     * 
     * @param user
     *            - a string
     */
    public void setUser(String user) {
        this.user = user;
    }

}
