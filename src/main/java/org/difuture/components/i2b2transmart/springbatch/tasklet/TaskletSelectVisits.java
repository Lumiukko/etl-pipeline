/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet;

import java.time.Period;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.difuture.components.i2b2transmart.configuration.ConfigurationVisitSelector;
import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.i2b2transmart.model.Subject;
import org.difuture.components.i2b2transmart.model.Timestamp;
import org.difuture.components.i2b2transmart.model.Visit;
import org.difuture.components.util.LoggerUtil;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

/**
 * Selects all visits of a period.
 * 
 * @author Claudia Lang
 *
 */
public class TaskletSelectVisits implements Tasklet {

    @Override
    public RepeatStatus execute(StepContribution contribution,
                                ChunkContext chunkContext) throws Exception {

        List<ConfigurationVisitSelector> selectors = Model.get().getConfig().getVisitSelectors();

        // If there is no visit selector configured, then the tasklet is
        // finished.
        if (selectors.isEmpty()) {
            LoggerUtil.info("No visit selector(s) configured or activated", this.getClass());
            return RepeatStatus.FINISHED;
        }

        LoggerUtil.info("Number of visits available in total: " + Model.get().getNumVisits(),
                        this.getClass());

        // Prepare
        Set<Visit> selectedVisits = new HashSet<>();

        // For each subject
        for (Subject subject : Model.get().getSubjects()) {

            // Prepare
            Timestamp dateFirstVisit = subject.getDateFirstVisit();
            Set<Visit> visitsForSubject = new HashSet<>();

            // For each configured visit selector
            for (ConfigurationVisitSelector selector : selectors) {

                // Prepare
                Visit selectedVisit = null;

                // For each visit
                Iterator<Visit> iterator = subject.getVisits().iterator();
                while (iterator.hasNext()) {

                    // Get next
                    Visit visit = iterator.next();

                    // Skip artificial visits
                    if (visit.isArtificial()) {
                        continue;
                    }

                    // The visit fits in the configured visit selector
                    if (isVisitInMargin(dateFirstVisit, visit.getStart(), selector)) {

                        // It matches! Never use it for anything else
                        iterator.remove();

                        // This is the first matching visit
                        if (selectedVisit == null) {

                            // Rename vist
                            selectedVisit = visit;
                            selectedVisit.setName(selector.getName());

                            // We found another matching visit
                        } else {

                            // Select the closest single visit
                            if (!ConfigurationVisitSelector.MERGE) {

                                // See which visit is closer to start
                                if (visit.getStart().isBefore(selectedVisit.getStart())) {
                                    selectedVisit = visit;
                                    selectedVisit.setName(selector.getName());
                                }

                                // Merge visit with existing visit
                            } else {

                                // Merge data into other visit
                                selectedVisit.merge(visit);
                            }
                        }
                    }
                }

                // Remember all visits that we selected for each subject
                if (selectedVisit != null) {
                    visitsForSubject.add(selectedVisit);
                }
            }

            // Always select the artificial visit
            Visit artificial = null;
            for (Visit visit : subject.getVisits()) {
                if (visit.isArtificial()) {
                    artificial = visit;
                    break;
                }
            }

            // Clear visits for subject
            subject.getVisits().clear();

            // Add, if enough visits found
            if (!ConfigurationVisitSelector.ALL_VISITS_REQUIRED ||
                visitsForSubject.size() == selectors.size()) {
                subject.getVisits().addAll(visitsForSubject);
                selectedVisits.addAll(visitsForSubject);
                if (artificial != null) {
                    subject.getVisits().add(artificial);
                    selectedVisits.add(artificial);
                }
            }
        }

        // Remove unselected visits
        Iterator<Visit> iterator = Model.get().getVisits().iterator();
        while (iterator.hasNext()) {
            if (!selectedVisits.contains(iterator.next())) {
                iterator.remove();
            }
        }

        // If no visit passed the configured visit selector,
        // log the error, throw an exception and do abort
        if (Model.get().getNumVisits() == 0) {
            LoggerUtil.fatalUnchecked("No visits found to pass Visits.Selector",
                                      this.getClass(),
                                      IllegalStateException.class);
        }

        // Done
        LoggerUtil.info("Visits selected according to visit selector: " +
                        Model.get().getNumVisits(),
                        this.getClass());
        return RepeatStatus.FINISHED;
    }

    /**
     * Returns true if the visit falls within the time frame configured by the
     * selector
     * 
     * @param dateFirstVisit
     *            - the date of the first visit
     * @param dateVisit
     *            - the date of the visit tested whether it is in the margin or
     *            not
     * @param selector
     *            - represents the configured visit selector
     * @return a boolean
     */
    private boolean isVisitInMargin(Timestamp dateFirstVisit,
                                    Timestamp dateVisit,
                                    ConfigurationVisitSelector selector) {
        if (dateFirstVisit == null || !dateFirstVisit.isValid()) { return false; }
        long diff = Period.between(dateFirstVisit.toLocalDate(), dateVisit.toLocalDate())
                          .toTotalMonths();
        return (diff >= selector.getStartMonth()) && (diff < selector.getEndMonth()); // Max
                                                                                      // is
                                                                                      // exclusive
    }
}
