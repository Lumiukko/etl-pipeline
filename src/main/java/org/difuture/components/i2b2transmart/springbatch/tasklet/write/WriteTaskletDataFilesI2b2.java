/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet.write;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.difuture.components.i2b2transmart.configuration.ConfigurationFileFormat;
import org.difuture.components.i2b2transmart.configuration.ConfigurationInputFile;
import org.difuture.components.i2b2transmart.configuration.ConfigurationInputFileNumberFormat;
import org.difuture.components.i2b2transmart.model.Data;
import org.difuture.components.i2b2transmart.model.Entity;
import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.i2b2transmart.model.OntologyNodeConcept;
import org.difuture.components.i2b2transmart.model.Subject;
import org.difuture.components.i2b2transmart.model.Timestamp;
import org.difuture.components.i2b2transmart.model.Timestamp.Reliability;
import org.difuture.components.i2b2transmart.model.Visit;
import org.difuture.components.util.FileUtil;
import org.difuture.components.util.LoggerUtil;

import com.carrotsearch.hppc.IntObjectOpenHashMap;

/**
 * Capsulates the i2b2 specific part
 * 
 * @author Claudia Lang
 *
 */
public class WriteTaskletDataFilesI2b2 extends WriteTaskletDataFiles {

    /**
     * Returns the header for the staging file
     * 
     * @param stagingOutputColumnNames
     * 
     * @return a string array
     */
    private String[] getHeader(List<String> stagingOutputColumnNames, boolean useMeasureTimestamp) {
        List<String> result = new ArrayList<>();
        // Identifying data
        result.add("___SUBJECT_ID___");
        result.add("___VISIT_ID___");
        result.add("___START___");
        result.add("___END___");
        // Observation data (concepts and modifiers)
        for (String hdr : stagingOutputColumnNames) {
            result.add(hdr);
        }

        // Measure for timestamps
        if (useMeasureTimestamp) {
            for (Reliability rel : Timestamp.Reliability.values()) {
                result.add(rel.toString());
            }
        }

        // Result
        return result.toArray(new String[result.size()]);
    }

    /**
     * Returns the names of columns in the associated staging file for this
     * concept. First returns columns for the concept (in case of categorical
     * data these are combinations of attribute and values) followed by columns
     * for the modifiers. This order is important.
     * 
     * @param config
     * @param concept
     * @param data
     * 
     * @return
     */
    private List<String> getOutputColumnNames(ConfigurationInputFile config, String concept) {

        // Prepare
        List<String> result = new ArrayList<>();
        Set<String> set = new HashSet<>();
        Entity attributeData = Model.get().getEntity(config);

        // Build list containing concept followed by all modifiers (for which
        // there is data)
        List<String> attributeNames = new ArrayList<String>();
        attributeNames.add(concept);
        for (String modifier : attributeData.getModifiers()) {
            if (isDataAvailableForAttribute(config, modifier)) {
                attributeNames.add(modifier);
            }
        }

        // For concept followed by modifiers
        for (String attributeName : attributeNames) {
            if (!attributeData.isAttributeDataTypeNumber(attributeName)) {
                for (Subject subject : Model.get().getSubjects()) {
                    for (Visit visit : subject.getVisits()) {

                        // Prepare
                        Set<Data> data = visit.getData(config);

                        // Skip if no data
                        if (data == null || data.isEmpty()) {
                            continue;
                        }

                        // Collect all column names (combinations of attribute
                        // and value)
                        for (Data dataPoint : data) {

                            // Store
                            String value = dataPoint.getValues().get(attributeName);
                            String key = FileUtil.escapeColumnName(attributeName, value);
                            if (value != null && !set.contains(key) && (!value.equals(""))) {
                                set.add(key);
                                result.add(key);
                            }
                        }
                    }
                }

            } else {

                // Numeric attributes remain as is
                result.add(attributeName);
            }
        }

        // Done
        return result;
    }

    /**
     * Writes the staging files for i2b2
     * 
     * @throws IOException
     * 
     */
    protected void write() {

        // Superroot - for ontology file for i2b2
        OntologyNodeConcept superRoot;
        Map<String, OntologyNodeConcept> fileConcept = new HashMap<String, OntologyNodeConcept>();

        // For each configuration input file
        for (ConfigurationInputFile config : Model.get().getConfig().getFiles()) {

            // Prepare
            ConfigurationInputFileNumberFormat numberConverter = config.getNumberConverter();
            int ignoredDueToParseErrors = 0;
            Entity attributeData = Model.get().getEntity(config);

            // Extract concepts and modifiers
            String inputFileName = config.getName();
            if (!fileConcept.containsKey(inputFileName)) {
                superRoot = new OntologyNodeConcept(inputFileName, inputFileName);
                fileConcept.put(inputFileName, superRoot);
            } else {
                superRoot = fileConcept.get(inputFileName);
            }

            Set<String> concepts = attributeData.getConcepts();

            // Track all output column names for each config
            Map<String, List<String>> conceptStagingOutputColumnNames = new HashMap<>();

            // For each concept
            for (String concept : concepts) {

                // Prepare
                List<String> attributeNames = getAttributeNames(config, concept);
                List<String> stagingOutputColumnNames = getOutputColumnNames(config, concept);
                conceptStagingOutputColumnNames.put(concept, stagingOutputColumnNames);

                // Prepare file name
                String fileName = FileUtil.escapeFileName(config, concept);
                fileName = folder.getAbsolutePath() + "/" + fileName;

                // Prepare csv writer
                CSVWriter csvWriter = new CSVWriter(fileName, true);

                String[] header = getHeader(stagingOutputColumnNames,
                                            Model.get()
                                                 .isI2b2() && config.isMeasureTimestampReliabilityToUse());
                Map<String, Integer> map = csvWriter.open(header);

                // Prepare log information
                long ignored = 0;
                long numberOfItemsWritten = 0;
                long ignoreVisitsWithoutSubject = 0;
                long numberOfItemsTotal = 0;
                long ignoredDueToEmptyValues = 0;
                long ignoredDueToNoArtificialVisitFound = 0;

                // Status
                LoggerUtil.info("Start writing file: " + fileName, this.getClass());

                // For each data point available for this config
                for (Subject subject : Model.get().getSubjects()) {
                    for (Visit visit : subject.getVisits()) {

                        // Prepare
                        Set<Data> data = visit.getData(config);

                        // Count each item found
                        numberOfItemsTotal++;

                        // Ignore empty values
                        if (data == null || data.isEmpty()) {
                            ignoredDueToEmptyValues++;
                            continue;
                        }

                        // Prepare
                        String stringSubjectID = visit.getSubject().getId().toString();
                        String stringVisitID = visit.getId().toString();

                        // For each data point
                        for (Data dataPoint : data) {

                            // Row to write
                            IntObjectOpenHashMap<String> row = new IntObjectOpenHashMap<>();

                            // Obtain timestamp: obtain start & end from visit
                            // if not known
                            Timestamp start = dataPoint.getStart();
                            Timestamp end = dataPoint.getEnd();
                            if (start == null) {
                                LoggerUtil.fatalChecked("No start date available for visit " +
                                                        stringVisitID + " (subject " +
                                                        stringSubjectID + ")",
                                                        WriteTaskletDataFilesI2b2.class,
                                                        RuntimeException.class);
                            }

                            // Start date is mandatory, end date is optional
                            String stringStart = start.format(ConfigurationFileFormat.getInstance()
                                                                                     .getOutputDateFormatter());
                            String stringEnd = end != null
                                    ? end.format(ConfigurationFileFormat.getInstance()
                                                                        .getOutputDateFormatter())
                                    : "";

                            // CSV writer supports addressing via column names
                            row.put(map.get("___SUBJECT_ID___"), stringSubjectID);
                            row.put(map.get("___VISIT_ID___"), stringVisitID);
                            row.put(map.get("___START___"), stringStart);
                            row.put(map.get("___END___"), stringEnd);

                            // Add measure for timestamp of the current data
                            // point
                            if (Model.get().isI2b2() &&
                                config.isMeasureTimestampReliabilityToUse()) {
                                String worstTimestampMeasure = (Timestamp.worst(start.getReliability(),
                                                                                end.getReliability())).toString();
                                row.put(map.get(worstTimestampMeasure), worstTimestampMeasure);
                            }

                            // For each attribute (may be a concept or a
                            // modifier)
                            boolean dataWritten = false;
                            for (String attribute : attributeNames) {

                                String attributeDataType = attributeData.getDataType(attribute);

                                // If there is no data type, there is no data
                                // for this attribute
                                if (attributeDataType == null) {
                                    continue;
                                }

                                // Get the value read for the i-th observation.
                                String value = dataPoint.getValues().get(attribute);
                                if (isEmpty(value)) {
                                    continue;
                                }

                                // Get the name of the column for the value.
                                String column = attribute;
                                boolean isAttributeNumeric = attributeDataType.equals("number");
                                if (!isAttributeNumeric) {
                                    // For a non numeric observation (attribute)
                                    // the column name gets the value as a
                                    // postfix.
                                    // This name will also be used as the name
                                    // of the concept in i2b2.
                                    column = attribute + "_" + value;
                                }

                                // Initialize the value to write in the staging
                                // file
                                if (isAttributeNumeric) {
                                    value = numberConverter.convert(value);
                                    ignoredDueToParseErrors += value == null ? 1 : 0;
                                    value = value == null ? "" : value;
                                }

                                // Add the data to write
                                row.put(map.get(column), value);
                                dataWritten = true;
                            }

                            // Write
                            if (dataWritten) {
                                csvWriter.write(row);
                            }
                        }

                        numberOfItemsWritten++;
                    }
                }

                // Close csv writer
                csvWriter.close();

                // TODO:
                // Delete, if no items to write were found
                /*
                 * if (numberOfItemsWritten == 0) { csvWriter.delete();
                 * StateOutputStaging.getInstance().removeOutputFileName(config,
                 * fileName); }
                 */

                // Log information
                LoggerUtil.info(" - End writing file: " + fileName, this.getClass());
                LoggerUtil.info(" - Items total: " + numberOfItemsTotal, this.getClass());
                LoggerUtil.info(" - Items written: " + numberOfItemsWritten, this.getClass());
                LoggerUtil.info(ignoredDueToEmptyValues,
                                " - Items ignored due to empty values: " + ignoredDueToEmptyValues,
                                this.getClass());
                LoggerUtil.info(ignored,
                                " - Items ignored due to missing mapping: " + ignored,
                                this.getClass());
                LoggerUtil.info(ignoreVisitsWithoutSubject,
                                " - Items ignored due to missing subject: " +
                                                            ignoreVisitsWithoutSubject,
                                this.getClass());
                LoggerUtil.info(ignoredDueToParseErrors,
                                " - Values ignored due to parsing exception: " +
                                                         ignoredDueToParseErrors,
                                this.getClass());
                LoggerUtil.info(ignoredDueToNoArtificialVisitFound,
                                " - Items ignored due to neither a proper visit nor an artificial visit was found: " +
                                                                    ignoredDueToNoArtificialVisitFound,
                                this.getClass());
            }

            // Store output column names for later use
            attributeData.setOutputColumnNames(conceptStagingOutputColumnNames);
        }
    }

}
