/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.difuture.components.i2b2transmart.configuration.ConfigurationInputFile;
import org.difuture.components.i2b2transmart.configuration.ConfigurationInputFileNumberFormat;
import org.difuture.components.i2b2transmart.model.Data;
import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.i2b2transmart.model.Subject;
import org.difuture.components.i2b2transmart.model.Visit;
import org.difuture.components.util.LoggerUtil;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

/**
 * Determines data types for each observation
 * 
 * TODO: Further types: blob and nlp TODO: Support units for numbers
 * 
 * @author Claudia Lang
 * 
 */
public class TaskletGetDataTypePerObservation implements Tasklet {

    @Override
    public RepeatStatus execute(StepContribution stepContribution,
                                ChunkContext chunkContext) throws Exception {

        // Log
        LoggerUtil.info("Detecting data types", this.getClass());

        // For each config
        for (ConfigurationInputFile config : Model.get().getConfig().getFiles()) {

            // Log
            LoggerUtil.info(" - Data type for entity: " + config.getName(), this.getClass());

            // Prepare
            ConfigurationInputFileNumberFormat numberConverter = config.getNumberConverter();
            Map<String, String> attributeToDataType = new HashMap<>();

            // For each data point available for this config
            for (Subject subject : Model.get().getSubjects()) {
                for (Visit visit : subject.getVisits()) {

                    // Prepare
                    Set<Data> data = visit.getData(config);

                    // Skip
                    if (data == null) {
                        continue;
                    }

                    // For each data point: TODO: optimize
                    for (Data dataPoint : data) {
                        for (Entry<String, String> entry : dataPoint.getValues().entrySet()) {
                            String attributeName = entry.getKey();
                            String attributeValue = entry.getValue();
                            if (attributeToDataType.get(attributeName) == null) {
                                attributeToDataType.put(attributeName,
                                                        getDataType(numberConverter,
                                                                    attributeValue));
                            } else if (!attributeToDataType.get(attributeName).equals("text")) {
                                attributeToDataType.put(attributeName,
                                                        getDataType(numberConverter,
                                                                    attributeValue));
                            }
                        }
                    }
                }
            }

            // Store
            Model.get().getEntity(config).setDataTypes(attributeToDataType);
        }

        // Done
        return RepeatStatus.FINISHED;
    }

    /**
     * Returns the data type
     * 
     * @param numberConverter
     * @param values
     * @return
     */
    private String getDataType(ConfigurationInputFileNumberFormat numberConverter, String values) {

        if (values == null || values.isEmpty()) {
            // TODO: Not perfect, but should work for now
            return "number";
        } else {
            if (!numberConverter.isNumber(values)) { return "text"; }
            return "number";
        }
    }
}
