/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet.write;

import java.io.File;
import java.util.ArrayList;

import org.difuture.components.i2b2transmart.configuration.ConfigurationFileFormat;
import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.i2b2transmart.model.Subject;
import org.difuture.components.i2b2transmart.springbatch.callback.SubjectFlatFileHeaderCallback;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.PassThroughLineAggregator;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.util.Assert;

/**
 * Writes subject data into a csv file.
 * 
 * @author Claudia Lang
 *
 */
public class WriteTaskletSubjects implements Tasklet, InitializingBean {

    /** Folder in which to write the files. */
    protected File folder;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(folder, "Folder for the subjects to write must not be null!");
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution,
                                ChunkContext chunkContext) throws Exception {
        writeSubjects(chunkContext);
        return RepeatStatus.FINISHED;
    }

    /**
     * Sets the folder.
     *
     * TODO: why do we need this method? Why not set this.folder from within the
     * constructor? TODO: Does spring batch requir default constructor only?
     * 
     * @param folder
     *            the new folder
     */
    public void setFolder(File folder) {
        this.folder = folder;
    }

    /**
     * Writes the subjects
     * 
     * @param chunkContext
     * @throws Exception
     */
    private void writeSubjects(ChunkContext chunkContext) throws Exception {
        // Write subjects
        File outputSubject;
        if (Model.get().isUseTmDataLoader()) {
            String studyID = Model.get().getConfig().getStudyID();
            outputSubject = new File(folder.getAbsolutePath() + "/" + studyID + "_subjects.txt");
        } else {
            outputSubject = new File(folder.getAbsolutePath() + "/___subjects___.csv");
        }
        FlatFileItemWriter<Subject> itemWriterSubject = new FlatFileItemWriter<Subject>();
        itemWriterSubject.setHeaderCallback(new SubjectFlatFileHeaderCallback());
        itemWriterSubject.setResource(new FileSystemResource(outputSubject));
        itemWriterSubject.setEncoding(ConfigurationFileFormat.getInstance().getCharset().name());
        itemWriterSubject.setLineSeparator(ConfigurationFileFormat.getInstance().getNewLine());
        itemWriterSubject.setLineAggregator(new PassThroughLineAggregator<Subject>());
        itemWriterSubject.open(chunkContext.getStepContext()
                                           .getStepExecution()
                                           .getJobExecution()
                                           .getExecutionContext());
        itemWriterSubject.write(new ArrayList<Subject>(Model.get().getSubjects()));
        itemWriterSubject.close();
    }

}
