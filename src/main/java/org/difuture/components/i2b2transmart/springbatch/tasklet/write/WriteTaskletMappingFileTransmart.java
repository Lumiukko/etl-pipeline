/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet.write;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.difuture.components.i2b2transmart.configuration.ConfigurationInputFile;
import org.difuture.components.i2b2transmart.configuration.ConfigurationSubject;
import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.i2b2transmart.model.OntologyNodeConcept;
import org.difuture.components.util.FileUtil;

/**
 * Writes the mapping file for component-transmart-batch
 * 
 * @author Claudia Lang
 * 
 */
public class WriteTaskletMappingFileTransmart extends WriteTaskletMappingFile {

    /** Number of the label column */
    private static final int COLUMN_NUMBER_LABEL = 3;

    /**
     * Escapes characters for the transmart mapping file
     * 
     * @param entityName
     * @return
     */
    private String escape(String value) {
        return value.replace("+", "\\+");
    }

    /**
     * Gets the entries for the concepts to the mapping file.
     */
    private List<String[]> getEntriesForConceptNodes() {
        List<String[]> result = new ArrayList<>();

        // Pos 0: subject id
        // Pos 1: visit name
        // Pos 2: label
        // Pos 3: value
        // Note: The indexes for the columns start in component transmart batch
        // at 1 instead of 0
        int indexMappingFileMod = 4;

        // For each input file and concept
        for (ConfigurationInputFile config : Model.get().getConfig().getFiles()) {

            // Get hierarchy
            Map<String, OntologyNodeConcept> conceptNodeHierarchie = Model.get()
                                                                          .getOntology()
                                                                          .getOntologiesForConcepts(config);

            // Get concepts
            Set<String> concepts = Model.get().getEntity(config).getConcepts();

            // Replace by summary
            if (!Model.get().isI2b2() && Model.get().getConfig().isSummary()) {
                concepts = Model.get().getEntity(config).getSummaryConcepts();
            }

            for (String concept : concepts) {
                String fileName = FileUtil.escapeFileName(config, concept);
                OntologyNodeConcept conceptNode = conceptNodeHierarchie.get(concept);

                // If there is no concept node, there is no data
                if (conceptNode == null) {
                    continue;
                }

                boolean visitTop = Model.get().getConfig().getVisitTop();
                boolean hasVisitsToWrite = !Model.get().getConfig().isSummary() &&
                                           (config.hasEncounter() ||
                                            config.isFindVisitViaTimestamp());
                String eavName = config.isEAV() ? config.getEAV().getAttribute() : null;
                result.addAll(getEntriesInMappingFilePlaceholderValues(fileName));
                result.add(getEntriesInMappingFileEntity(fileName,
                                                         indexMappingFileMod,
                                                         config.getName(),
                                                         config.path,
                                                         concept,
                                                         conceptNode.getDataType().equals("N")
                                                                 ? "NUMERICAL"
                                                                 : "CATEGORICAL",
                                                         visitTop,
                                                         hasVisitsToWrite,
                                                         eavName));
            }
        }

        return result;
    }

    /**
     * Creates a new line to add to the file mapping.params
     * 
     * @param fileName
     *            - the name of the output file
     * @param columnNumber
     *            - the position of the attribute, starting at 1
     * @param entityName
     *            - the name of the entity
     * @param path
     *            - a path to add in the hiearchie
     * @param attribute
     *            - the attribute to write (concept)
     * @param nodeType
     *            - the type of the attribute (numerical or categorical)
     * @param writeVisitAtTop
     *            - configures the position of the visit
     * @param recordsWithVisit
     *            - if there is visit
     * @param eavName
     *            - the name of the eav entity, if this entity is a generic one
     * @return a string array
     */
    // NOTE: To use while modifiers are not supported
    private String[] getEntriesInMappingFileEntity(String fileName,
                                                   int columnNumber,
                                                   String entityName,
                                                   String path,
                                                   String attribute,
                                                   String nodeType,
                                                   boolean writeVisitAtTop,
                                                   boolean recordsWithVisit,
                                                   String eavName) {

        String categoryCode = "Characteristics";

        if (!recordsWithVisit) {
            if (path != null) {
                categoryCode = categoryCode + "+" + path;
            }

            if (eavName != null) {
                categoryCode = categoryCode + "+" + escape(eavName);
            }
            categoryCode += "+" + escape(entityName);

        } else {
            if (writeVisitAtTop) {
                categoryCode += "+VISITNAME";
            }

            if (path != null) {
                categoryCode = categoryCode + "+" + path;
            }

            if (eavName != null) {
                categoryCode = categoryCode + "+" + escape(eavName);
            }

            categoryCode = categoryCode + "+" + escape(entityName);

            if (!writeVisitAtTop) {
                categoryCode = categoryCode + "+VISITNAME";
            }
        }

        return getRow(fileName,
                      categoryCode,
                      columnNumber,
                      "\\",
                      String.valueOf(COLUMN_NUMBER_LABEL),
                      "",
                      nodeType);
    }

    /**
     * Gets the entries for the placeholders in the mapping file. Place holders
     * are all but the observation data. Examples for place holders: - Subject -
     * Visit - Label
     * 
     * @param fileName
     */
    private List<String[]> getEntriesInMappingFilePlaceholderValues(String fileName) {
        List<String[]> result = new ArrayList<>();
        int column = 1;

        result.add(getRow(fileName, "Subjects", column++, "SUBJ_ID", null, null, null));
        result.add(getRow(fileName, "Characteristics", column++, "VISIT_NAME", null, null, null));
        result.add(getRow(fileName,
                          "Characteristics",
                          COLUMN_NUMBER_LABEL,
                          "DATA_LABEL",
                          null,
                          null,
                          null));

        return result;
    }

    /**
     * Gets the entries for the subjects in the mapping file
     * 
     * @param mapping
     */
    private List<String[]> getEntriesInMappingFileSubjects() {
        List<String[]> result = new ArrayList<>();

        // Prepare
        ConfigurationSubject config = Model.get().getConfig().getSubjectConfiguration();
        String pathSubject = "Characteristics+" + config.path;
        String fileName = "___subjects___.csv";

        result.add(getRow(fileName, "Subjects", 1, "SUBJ_ID", null, null, null));

        // Write columns
        int column = 2;
        if (config.sex != null) {
            result.add(getRow(fileName, pathSubject, column++, "Sex", null, null, null));
        }
        if (config.zip != null) {
            result.add(getRow(fileName, pathSubject, column++, config.zip, null, null, null));
        }
        if (config.age != null) {
            result.add(getRow(fileName, pathSubject, column++, "Age", null, null, null));
        }
        if (config.birthYear != null) {
            result.add(getRow(fileName, pathSubject, column++, config.birthYear, null, null, null));
        }
        if (config.birthMonth != null) {
            result.add(getRow(fileName,
                              pathSubject,
                              column++,
                              config.birthMonth,
                              null,
                              null,
                              null));
        }
        if (config.birthDate != null) {
            result.add(getRow(fileName, pathSubject, column++, config.birthDate, null, null, null));
        }
        if (config.loadIDs) {
            result.add(getRow(fileName, pathSubject, column++, "ID", null, null, null));
        }

        return result;
    }

    /**
     * 
     * @param filename
     * @param category
     * @param offset
     * @param label
     * @param labelSource
     * @param controlledVocabCd
     * @param type
     * @return a string array
     */
    private String[] getRow(String filename,
                            String category,
                            int offset,
                            String label,
                            String labelSource,
                            String controlledVocabCd,
                            String type) {
        List<String> result = new ArrayList<>();

        result.add(filename);
        result.add(category);
        result.add(String.valueOf(offset));
        result.add(label);

        if (labelSource != null) {
            result.add(labelSource);
            result.add(controlledVocabCd);
            result.add(type);
        }

        return result.toArray(new String[result.size()]);
    }

    /**
     * Gets all rows to write in the mapping file
     * 
     * @return a list of string arrays
     */
    @Override
    protected List<String[]> getEntriesToMappingFile() {
        List<String[]> result = new ArrayList<>();

        result.addAll(getEntriesInMappingFileSubjects());
        result.addAll(getEntriesForConceptNodes());

        return result;
    }

    /**
     * Returns the header for the mapping file
     * 
     * @return a string array
     */
    @Override
    protected String[] getHeader() {
        return new String[] { "Filename",
                              "Category code",
                              "Column number",
                              "Data label",
                              "Data label source",
                              "Controlled vocabulary CD",
                              "Concept type" };
    }
}
