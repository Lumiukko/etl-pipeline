/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.callback;

import java.io.IOException;
import java.io.Writer;

import org.difuture.components.i2b2transmart.configuration.ConfigurationFileFormat;
import org.difuture.components.i2b2transmart.configuration.ConfigurationSubject;
import org.difuture.components.i2b2transmart.model.Model;
import org.springframework.batch.item.file.FlatFileHeaderCallback;

/**
 * Writes the header in a tab delimited csv file.
 * 
 * @author Claudia Lang
 * 
 */
public class SubjectFlatFileHeaderCallback implements FlatFileHeaderCallback {

    @Override
    public void writeHeader(Writer writer) throws IOException {
        // Prepare
        ConfigurationSubject config = Model.get().getConfig().getSubjectConfiguration();
        StringBuilder header = new StringBuilder();
        String delimiter = ConfigurationFileFormat.getInstance().getDelimiterOutput();

        // Add studyID for tmDataLoader
        if (Model.get().isUseTmDataLoader()) {
            header.append("STUDY_ID");
            header.append(delimiter);
            header.append("SUBJ_ID");
        } else {
            header.append("___SUBJECT_ID___");
        }

        if (config.sex != null) {
            header.append(delimiter);
            header.append(config.sex);
        }
        if (config.zip != null) {
            header.append(delimiter);
            header.append(config.zip);
        }
        if (config.age != null) {
            header.append(delimiter);
            header.append(config.age);
        }
        if (!Model.get().isI2b2()) {
            if (config.birthYear != null) {
                header.append(delimiter);
                header.append(config.birthYear);
            }
            if (config.birthMonth != null) {
                header.append(delimiter);
                header.append(config.birthMonth);
            }
        }
        if (config.birthDate != null) {
            header.append(delimiter);
            header.append(config.birthDate);
        }
        if (config.loadIDs) {
            header.append(delimiter);
            header.append("ID");
        }
        writer.write(header.toString());
    }

}
