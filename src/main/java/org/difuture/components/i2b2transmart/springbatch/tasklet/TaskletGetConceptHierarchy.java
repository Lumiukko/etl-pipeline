/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.difuture.components.i2b2transmart.configuration.ConfigurationInputFile;
import org.difuture.components.i2b2transmart.model.Data;
import org.difuture.components.i2b2transmart.model.Entity;
import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.i2b2transmart.model.OntologyNodeConcept;
import org.difuture.components.i2b2transmart.model.OntologyNodeModifier;
import org.difuture.components.i2b2transmart.model.Subject;
import org.difuture.components.i2b2transmart.model.Timestamp;
import org.difuture.components.i2b2transmart.model.Timestamp.Reliability;
import org.difuture.components.i2b2transmart.model.Visit;
import org.difuture.components.util.FileUtil;
import org.difuture.components.util.LoggerUtil;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

/**
 * Computes the concept tree
 * 
 * @author Claudia Lang
 * 
 */
public class TaskletGetConceptHierarchy implements Tasklet {

    @Override
    public RepeatStatus execute(StepContribution stepContribution,
                                ChunkContext chunkContext) throws Exception {

        LoggerUtil.info("Computing ontology tree", this.getClass());

        // For managing ontologies that need to be stored
        Map<String, OntologyNodeConcept> ontologiesToWrite = new HashMap<String, OntologyNodeConcept>();

        // Internal: for managing common roots across entities
        Map<String, OntologyNodeConcept> eavParentToConceptNode = new HashMap<String, OntologyNodeConcept>();

        // Internal: for managing common roots across entities
        Map<String, OntologyNodeConcept> pathParentToConceptNode = new HashMap<String, OntologyNodeConcept>();

        // Internal: for mapping entities to concept nodes
        Map<String, OntologyNodeConcept> entityToConceptNode = new HashMap<String, OntologyNodeConcept>();

        // Note: It is important, that the generic entities are managed after
        // the configured entities
        for (ConfigurationInputFile config : Model.get().getConfig().getFiles()) {

            // Log
            LoggerUtil.info(" - Ontology tree for entity: " + config.getName(), this.getClass());

            // Prepare
            Map<String, OntologyNodeConcept> conceptMap = new HashMap<>();
            Map<String, OntologyNodeConcept> conceptNodeHierarchie = new HashMap<>();
            String entityName = config.getName();
            String path = config.getPath();

            // Determine node for entity
            OntologyNodeConcept entityNode = entityToConceptNode.get(entityName);
            if (entityNode == null) {

                // If EAV
                if (config.isEAV()) {

                    // Create parent node
                    OntologyNodeConcept eavParentNode = eavParentToConceptNode.get(config.getEAV()
                                                                                         .getAttribute());
                    if (eavParentNode == null) {

                        // Insert parent, if needed
                        OntologyNodeConcept pathParentNode = null;
                        if (path != null) {
                            pathParentNode = pathParentToConceptNode.get(path);
                            if (pathParentNode == null) {
                                pathParentNode = new OntologyNodeConcept(path, path);
                                pathParentToConceptNode.put(path, pathParentNode);
                                ontologiesToWrite.put(path, pathParentNode);
                            }
                        }

                        // Create EAV parent with path
                        if (pathParentNode != null) {

                            eavParentNode = new OntologyNodeConcept(config.getEAV().getAttribute(),
                                                                    pathParentNode,
                                                                    config.getEAV().getAttribute(),
                                                                    false,
                                                                    false);
                            eavParentToConceptNode.put(config.getEAV().getAttribute(),
                                                       eavParentNode);

                            // Create EAV parent without path
                        } else {

                            eavParentNode = new OntologyNodeConcept(config.getEAV().getAttribute(),
                                                                    config.getEAV().getAttribute());
                            eavParentToConceptNode.put(config.getEAV().getAttribute(),
                                                       eavParentNode);
                            ontologiesToWrite.put(config.getEAV().getAttribute(), eavParentNode);
                        }
                    }

                    // Create node and store
                    entityNode = new OntologyNodeConcept(entityName,
                                                         eavParentNode,
                                                         entityName,
                                                         false,
                                                         false);
                    entityToConceptNode.put(entityName, entityNode);

                } else {

                    // Insert parent, if needed
                    OntologyNodeConcept pathParentNode = null;
                    if (path != null) {
                        pathParentNode = pathParentToConceptNode.get(path);
                        if (pathParentNode == null) {
                            pathParentNode = new OntologyNodeConcept(path, path);
                            pathParentToConceptNode.put(path, pathParentNode);
                            ontologiesToWrite.put(path, pathParentNode);
                        }
                    }
                    // Create node with path
                    if (pathParentNode != null) {

                        entityNode = new OntologyNodeConcept(entityName,
                                                             pathParentNode,
                                                             entityName,
                                                             false,
                                                             false);
                        entityToConceptNode.put(entityName, entityNode);

                        // Create node without path
                    } else {

                        entityNode = new OntologyNodeConcept(entityName, entityName);
                        entityToConceptNode.put(entityName, entityNode);
                        ontologiesToWrite.put(entityName, entityNode);
                    }
                }
            }

            // TODO: Simplify. If the concept is numeric, is it necessary to
            // iterate over all attributes entries?
            Entity attributeData = Model.get().getEntity(config);
            Set<String> concepts = attributeData.getConcepts();

            // Replace by summary
            if (!Model.get().isI2b2() && Model.get().getConfig().isSummary()) {
                concepts = attributeData.getSummaryConcepts();
            }

            // For concepts
            for (String concept : concepts) {

                // Prepare
                String dataType = attributeData.getDataType(concept);
                String fileName = FileUtil.escapeFileName(config, concept);

                // If there is no data, there is no data type
                if (dataType == null) {
                    continue;

                }

                // The property EAV.Explicit is supported for numerical concepts
                // only
                if (config.isEAV() && config.getEAV().isExplicit()) {
                    if (!dataType.equals("number")) {
                        // Option EAV.Excplicit makes sense for non-numeric
                        // concepts, only.
                        LoggerUtil.warn("Property " + config.getName() +
                                        ".EAV.Explicit was configured for a non-numeric concept: " +
                                        concept,
                                        this.getClass());
                    }
                }
                // Get name of the concept
                String conceptNodeName = (config.isEAV() && !config.getEAV().isExplicit())
                        ? concept + " " + entityName
                        : concept;
                // Create concept
                OntologyNodeConcept conceptNode = new OntologyNodeConcept(conceptNodeName,
                                                                          entityNode,
                                                                          conceptNodeName,
                                                                          dataType.equals("number"),
                                                                          true);

                // Add modifier as measure for timestamp reliability
                if (Model.get().isI2b2() && config.isMeasureTimestampReliabilityToUse()) {
                    OntologyNodeModifier modNode = new OntologyNodeModifier(fileName,
                                                                            conceptNode,
                                                                            "Zuverlässigkeit Zeitstempel",
                                                                            false,
                                                                            conceptNode);
                    for (Reliability rel : Timestamp.Reliability.values()) {
                        OntologyNodeModifier tmpNode = new OntologyNodeModifier(fileName,
                                                                                modNode,
                                                                                rel.toString(),
                                                                                false,
                                                                                conceptNode);
                        conceptMap.put(rel.toString(), tmpNode);
                    }
                }

                // Build list of attribute names to consider
                List<String> attributeNames = new ArrayList<String>();
                attributeNames.add(concept);
                for (String modifier : Model.get().getEntity(config).getModifiers()) {
                    if (isDataAvailableForAttribute(modifier, config)) {
                        attributeNames.add(modifier);
                    }
                }

                // For each data point available for this config
                for (Subject subject : Model.get().getSubjects()) {
                    for (Visit visit : subject.getVisits()) {

                        // Prepare
                        Set<Data> data = visit.getData(config);
                        OntologyNodeConcept root;

                        // Ignore empty data
                        if (data == null || data.isEmpty()) {
                            continue;
                        }

                        // For each attribute name
                        for (String attributeName : attributeNames) {

                            // Prepare
                            String attributeType = attributeData.getDataType(attributeName);

                            // If there is no data type, there is no data for
                            // this attribute
                            if (attributeType == null) {
                                continue;
                            }

                            boolean attributeTypeIsNumeric = attributeType.equals("number");
                            if (attributeName.equals(concept)) {
                                root = conceptNode;
                                // Add the concept for a numerical observation
                                // here
                                // The concepts for a non-numerical observation
                                // depend on the values
                                // of the observation and are added below.
                                if (attributeType.equals("number")) {
                                    conceptMap.put(attributeName, conceptNode);
                                }
                            } else {
                                root = conceptMap.get(attributeName);
                                if (root == null) {
                                    root = new OntologyNodeModifier(fileName,
                                                                    conceptNode,
                                                                    attributeName,
                                                                    attributeTypeIsNumeric,
                                                                    conceptNode);
                                    conceptMap.put(attributeName, root);
                                }
                            }

                            // Create nodes for categorical values
                            if (!attributeTypeIsNumeric) {

                                // Write values
                                for (Data dataPoint : data) {

                                    // Get value
                                    String value = dataPoint.getValues().get(attributeName);

                                    // Skip empty value
                                    if (isEmpty(value)) {
                                        continue;
                                    }

                                    // Calculate name
                                    String observationString = FileUtil.escapeColumnName(attributeName,
                                                                                         value);

                                    // Add node
                                    addValueNode(conceptMap,
                                                 attributeName,
                                                 concept,
                                                 observationString,
                                                 root,
                                                 value,
                                                 fileName,
                                                 conceptNode);
                                }
                            }
                        }
                    }
                    conceptNodeHierarchie.put(concept, conceptNode);
                }
            }

            // Store
            Model.get().getOntology().setOntologyNodesForOutputColumns(config, conceptMap);
            Model.get().getOntology().setOntologiesForConcepts(config, conceptNodeHierarchie);
        }

        // Store
        Model.get().getOntology().setOntologyFiles(ontologiesToWrite);

        // Done
        return RepeatStatus.FINISHED;
    }

    /**
     * For a non numerical observation a new NodeConcept object is created and
     * put to the conceptMap. This method has side effects! It adds data to the
     * provided concept map.
     * 
     * @param conceptMap
     * @param observation
     * @param concept
     * @param observationString
     * @param root
     * @param value
     * @param fileName
     * @param conceptNode
     */
    private void addValueNode(Map<String, OntologyNodeConcept> conceptMap,
                              String observation,
                              String concept,
                              String observationString,
                              OntologyNodeConcept root,
                              String value,
                              String fileName,
                              OntologyNodeConcept conceptNode) {

        if (observation.equals(concept)) {
            OntologyNodeConcept node = conceptMap.get(observationString);
            if (node == null) {
                node = new OntologyNodeConcept(observation, root, value, false, false);
                conceptMap.put(observationString, node);
            }
        } else {

            OntologyNodeModifier node = (OntologyNodeModifier) conceptMap.get(observationString);
            if (node == null) {
                // If the modifiers are without any hierarchie (i.e. all
                // modifiers are to be on level 1) then use observationString
                // instead of value
                node = new OntologyNodeModifier(fileName, root, value, false, conceptNode);
                conceptMap.put(observationString, node);
            }
        }
    }

    /**
     * Gets the distinct values per attribute.
     *
     * @param attribute
     *            the name of the attribute
     * @param config
     *            a Map containing the Attributes objects
     * @return HashSet
     */
    private boolean isDataAvailableForAttribute(String attribute, ConfigurationInputFile config) {
        for (Subject subject : Model.get().getSubjects()) {
            for (Visit visit : subject.getVisits()) {

                // Get
                Set<Data> data = visit.getData(config);

                // Skip
                if (data == null) {
                    continue;
                }

                // For each data point
                for (Data dataPoint : data) {
                    if (dataPoint.getValues().containsKey(attribute)) { return true; }
                }
            }
        }
        return false;
    }

    /**
     * Returns whether the value is empty
     * 
     * @param value
     * @return
     */
    private boolean isEmpty(String value) {
        return value == null || value.equals("");
    }
}
