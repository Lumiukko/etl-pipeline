/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet.write;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import org.difuture.components.i2b2transmart.configuration.ConfigurationSubject;
import org.difuture.components.i2b2transmart.model.Attribute;
import org.difuture.components.i2b2transmart.model.AttributeAge;
import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.i2b2transmart.model.OntologyNodeConcept;
import org.difuture.components.i2b2transmart.model.Subject;
import org.difuture.components.util.FileUtil;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

/**
 * Writes the i2b2-specific ontologies into xml files.
 * 
 * @author Claudia Lang
 * @author Fabian Prasser
 */
public class WriteTaskletOntology implements Tasklet, InitializingBean {

    /** Folder in which to write the files. */
    private File folder;

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(folder, "Folder for the ontology to write must not be null!");
    }

    @Override
    public RepeatStatus execute(StepContribution arg0, ChunkContext arg1) throws Exception {
        Map<String, OntologyNodeConcept> ontologiesToSerialize = Model.get()
                                                                      .getOntology()
                                                                      .getOntologyFiles();
        for (Entry<String, OntologyNodeConcept> ontology : ontologiesToSerialize.entrySet()) {
            File file = new File(folder.getAbsolutePath() + "/" +
                                 FileUtil.escapeFileName(ontology.getKey()) + "___ontology.xml");
            OntologyNodeConcept root = ontology.getValue();
            root.pack();
            root.serialize(file);
        }
        writeSubjectsOntologyI2B2();
        return RepeatStatus.FINISHED;
    }

    /**
     * Sets the folder.
     *
     * <BR>
     * TODO why do we need this method? Why not set this.folder from within the
     * constructor? Spring Batch support default constructor only?
     * 
     * @param folder
     *            the new folder
     */
    public void setFolder(File folder) {
        this.folder = folder;
    }

    /**
     * Creates the entries in the ontology file for i2b2.
     * 
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private void writeSubjectsOntologyI2B2() throws IOException {

        ConfigurationSubject config = Model.get().getConfig().getSubjectConfiguration();

        OntologyNodeConcept root = new OntologyNodeConcept(config.path, config.path);

        // Concept gender
        OntologyNodeConcept concept = new OntologyNodeConcept("Gender", root, "Gender", false);
        if (config.sex != null) {
            new OntologyNodeConcept(concept, "Male", "DEM|SEX:m", false);
            new OntologyNodeConcept(concept, "Female", "DEM|SEX:f", false);
            new OntologyNodeConcept(concept, "Unknown", "DEM|SEX:u", false);
        } else {
            new OntologyNodeConcept(concept, "Not recorded", "DEM|SEX:@", false);
        }

        // Concept zip
        if (config.zip != null) {
            new OntologyNodeConcept("Zip code", root, "Zip code", true);
        }

        // Concept age
        // NOTE: Concept age is available in breakdown-queries so it's created
        // either way,
        // otherwise the age-patient-breakdown-query will lead to an error in
        // i2b2
        OntologyNodeConcept conceptAge = new OntologyNodeConcept("Age", root, "Age", false);
        OntologyNodeConcept age = new OntologyNodeConcept(conceptAge,
                                                          "0-9 years old",
                                                          "DEM|AGE:0_9",
                                                          false);
        for (int i = 0; i <= 9; i++) {
            new OntologyNodeConcept(age, i + " years old", "DEM|AGE:" + i, false);
        }

        // 10-17 years old
        age = new OntologyNodeConcept(conceptAge, "10-17 years old", "DEM|AGE:10_17", false);
        for (int i = 10; i <= 17; i++) {
            new OntologyNodeConcept(age, i + " years old", "DEM|AGE:" + i, false);
        }
        // 18-34 years old
        age = new OntologyNodeConcept(conceptAge, "18-34 years old", "DEM|AGE:18_34", false);
        for (int i = 18; i <= 34; i++) {
            new OntologyNodeConcept(age, i + " years old", "DEM|AGE:" + i, false);
        }
        // 35-44 years old
        age = new OntologyNodeConcept(conceptAge, "35-44 years old", "DEM|AGE:35_44", false);
        for (int i = 35; i <= 44; i++) {
            new OntologyNodeConcept(age, i + " years old", "DEM|AGE:" + i, false);
        }
        // 45-54 years old
        age = new OntologyNodeConcept(conceptAge, "45-54 years old", "DEM|AGE:45_54", false);
        for (int i = 45; i <= 54; i++) {
            new OntologyNodeConcept(age, i + " years old", "DEM|AGE:" + i, false);
        }
        // 55-64 yeras old
        age = new OntologyNodeConcept(conceptAge, "55-64 years old", "DEM|AGE:55_64", false);
        for (int i = 55; i <= 64; i++) {
            new OntologyNodeConcept(age, i + " years old", "DEM|AGE:" + i, false);
        }
        // 65-74 years old
        age = new OntologyNodeConcept(conceptAge, "65-74 years old", "DEM|AGE:65_74", false);
        for (int i = 65; i <= 74; i++) {
            new OntologyNodeConcept(age, i + " years old", "DEM|AGE:" + i, false);
        }
        // 75-84 years old
        age = new OntologyNodeConcept(conceptAge, "75-84 years old", "DEM|AGE:75_84", false);
        for (int i = 75; i <= 84; i++) {
            new OntologyNodeConcept(age, i + " years old", "DEM|AGE:" + i, false);
        }
        // >= 85 years old
        age = new OntologyNodeConcept(conceptAge, ">= 85 years old", "DEM|AGE:GE_85", false);
        for (int i = 85; i <= AttributeAge.MAX_VALUE; i++) {
            new OntologyNodeConcept(age, i + " years old", "DEM|AGE:" + i, false);
        }
        new OntologyNodeConcept(conceptAge, Attribute.UNKNOWN, "DEM|AGE:u", false);
        new OntologyNodeConcept(conceptAge, Attribute.NOT_RECORDED, "DEM|AGE:@", false);

        if (config.language != null) {
            new OntologyNodeConcept("Language", root, "Language", false);
        }

        if (config.maritalStatus != null) {
            new OntologyNodeConcept("Marital status", root, "Marital status", false);
        }

        if (config.religion != null) {
            new OntologyNodeConcept("Religion", root, "Religion", false);
        }

        if (config.stateCityZipPath != null) {
            new OntologyNodeConcept("State city path", root, "State city path", false);
        }

        if (config.income != null) {
            new OntologyNodeConcept("INCOME", root, "INCOME", true);
        }

        // Concept vital status
        // NOTE: Concept vital status is available in breakdown-queries so it's
        // created either way,
        // otherwise the vital-status-patient-breakdown-query will lead to an
        // error in i2b2
        OntologyNodeConcept conceptVitalStatus = new OntologyNodeConcept("Vital status",
                                                                         root,
                                                                         "Vital status",
                                                                         false);
        if (config.vitalStatus == null) {
            new OntologyNodeConcept(conceptVitalStatus, "Not recorded", "DEM|VITAL:@", false);
        }

        // Concept race
        // NOTE: Concept race is available in breakdown-queries so it's created
        // either way,
        // otherwise the race-patient-breakdown-query will lead to an error in
        // i2b2
        OntologyNodeConcept conceptRace = new OntologyNodeConcept("Race", root, "Race", false);
        if (config.race == null) {
            new OntologyNodeConcept(conceptRace, "Not recorded", "DEM|RACE:@", false);
        }

        // Concepts for IDs
        if (config.loadIDs) {
            OntologyNodeConcept conceptID = new OntologyNodeConcept("ID", root, "ID", false);
            for (Subject subject : Model.get().getSubjects()) {
                String id = subject.getId().toString();
                new OntologyNodeConcept(conceptID, id, "SYS|ID:" + id, false);
            }
        }

        // Save ontology
        root.serialize(new File(folder.getAbsolutePath() + "/__subjects__.csv___ontology.xml"));
    }
}
