/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet.read;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.difuture.components.i2b2transmart.configuration.ConfigurationInputFile;
import org.difuture.components.i2b2transmart.configuration.ConfigurationValueMapping;
import org.difuture.components.i2b2transmart.model.Data;
import org.difuture.components.i2b2transmart.model.Entity;
import org.difuture.components.i2b2transmart.model.Key;
import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.i2b2transmart.model.Subject;
import org.difuture.components.i2b2transmart.model.Timestamp;
import org.difuture.components.i2b2transmart.model.Visit;
import org.difuture.components.util.LoggerUtil;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

/**
 * Reads all data
 * 
 * @author Claudia Lang
 * @author Fabian Prasser
 */
public class ReadTaskletDataItem implements Tasklet {

    /**
     * Class holding basic file data
     * 
     * @author Claudia Lang
     * @author Fabian Prasser
     */
    private static class InputFileData {
        /** The index of the column with the start date */
        private int               startIndex      = -2;
        /** The index of the column with the end date */
        private int               endIndex        = -2;
        /** The index of the column with the day of the start date */
        private int               indexStartDay   = -2;
        /** The index of the column with the month of the start date */
        private int               indexStartMonth = -2;
        /** The index of the column with the year of the start date */
        private int               indexStartYear  = -2;
        /** The index of the column with the day of the end date */
        private int               indexEndDay     = -2;
        /** The index of the column with the month of the end date */
        private int               indexEndMonth   = -2;
        /** The index of the column with the year of the end date */
        private int               indexEndYear    = -2;
        /** The formatter for the start date and end date */
        private DateTimeFormatter dateType        = null;
    }

    @Override
    public RepeatStatus execute(StepContribution contribution,
                                ChunkContext chunkContext) throws Exception {

        // Iterate over input files
        Iterator<ConfigurationInputFile> it = Model.get().getConfig().getFiles().iterator();
        while (it.hasNext()) {

            // Prepare
            int mappedByTimestamp = 0;
            int ignoredDueToMissingVisit = 0;
            int ignoredDueToMissingTimestamp = 0;
            ConfigurationInputFile config = it.next();

            // Log
            LoggerUtil.info("Entity " + config.getName() + ": reading payload data from file " +
                            config.getFileNames(),
                            this.getClass());

            // Configure all attributes for this file
            if (config.isEAV()) {
                CSVReader.loadIntoCache(config.file, config.charset);
            }
            Entity attributes = getFileAttributeData(config);
            Model.get().setEntity(config, attributes);

            // Read csv file
            List<String[]> rows = CSVReader.getRows(config.getMapping(),
                                                    attributes.getColumnsToRead(),
                                                    config.file,
                                                    config.getColumnFilter(),
                                                    config.charset);
            LoggerUtil.info(" - Size: " + rows.size() + " rows with " + rows.get(0).length +
                            " columns read from file",
                            this.getClass());

            // Filter out empty entities
            if (rows.size() <= 1) {
                it.remove();
                LoggerUtil.info(" - Entity has been ignored because because it is empty",
                                this.getClass());
                continue;
            }

            // Handle the header
            List<String> header = Arrays.asList(rows.get(0));
            try {
                config.getKey().calculateIndexes(header);
            } catch (Exception e) {
                LoggerUtil.fatalUnchecked("Files in question: " + config.getFileNames(),
                                          e,
                                          this.getClass(),
                                          IllegalStateException.class);
            }
            InputFileData offsets = getAttributeOffsets(header, config);

            // Prepare map
            int index = 0;
            Map<String, Integer> map = new HashMap<>();
            for (String attribute : header) {
                map.put(attribute, index++);
            }

            // The first line contains the column names, and these are not
            // needed here
            for (int i = 1; i < rows.size(); i++) {

                // Extract line
                String[] row = rows.get(i);

                // Perform value mapping
                for (ConfigurationValueMapping mapping : config.valueMappings.values()) {
                    if (map.containsKey(mapping.getAttribute())) {
                        int column = map.get(mapping.getAttribute());
                        row[column] = mapping.map(row[column]);
                    }
                }

                // Start and end dates
                Timestamp __start = null;
                Timestamp __end = null;
                if (offsets.dateType != null) {

                    if (!config.hasSeveralColumnsForStartDate()) {

                        String start = offsets.startIndex == -1 ? null : row[offsets.startIndex];
                        String end = offsets.endIndex == -1 ? null : row[offsets.endIndex];
                        __start = new Timestamp(offsets.dateType, start, true);
                        if (!__start.isValid()) {
                            ignoredDueToMissingTimestamp++;
                            continue;
                        }
                        __end = new Timestamp(offsets.dateType, end, false);

                    } else {

                        // Read
                        String _startDay = offsets.indexStartDay == -1 ? "1"
                                : row[offsets.indexStartDay];
                        String _startMonth = row[offsets.indexStartMonth];
                        String _startYear = row[offsets.indexStartYear];

                        // Parse
                        try {
                            int iStartDay = Integer.valueOf(_startDay);
                            int iStartMonth = Integer.valueOf(_startMonth);
                            int iStartYear = Integer.valueOf(_startYear);
                            __start = new Timestamp(iStartDay, iStartMonth, iStartYear, true);
                        } catch (NumberFormatException e) {
                            __start = null;
                            continue;
                        }

                        // Check
                        if (__start == null || !__start.isValid()) {
                            ignoredDueToMissingTimestamp++;
                            continue;
                        }

                        // Read
                        String _endDay = offsets.indexEndDay == -1 ? "1" : row[offsets.indexEndDay];
                        String _endMonth = row[offsets.indexEndMonth];
                        String _endYear = row[offsets.indexEndYear];

                        // Parse
                        try {
                            int iEndDay = Integer.valueOf(_endDay);
                            int iEndMonth = Integer.valueOf(_endMonth);
                            int iEndYear = Integer.valueOf(_endYear);
                            __end = new Timestamp(iEndDay, iEndMonth, iEndYear, false);
                        } catch (NumberFormatException e) {
                            __end = null;
                        }
                    }
                }

                // Set end date
                if (__end == null || __end.isValid()) {
                    __end = __start;
                }

                // The key is either the key for the encounter
                // (FileX.Encounter=<Name> is configured)
                // or for the subject (FileX.Subject=<Name> is configured)
                // -> This should be made explicit (getEncounter() and
                // getSubject())

                // Prepare
                Key encounterOrSubjectID = config.getKey().getKey(row);
                Key encounterID = config.hasEncounter() ? encounterOrSubjectID
                        : encounterOrSubjectID.getArtificalVisitKey();
                Set<Visit> visits = new HashSet<>();

                // Attach to single visit defined in file
                if (!config.isFindVisitViaTimestamp()) {
                    Visit found = Model.get().getVisit(encounterID);
                    if (found != null) {
                        visits.add(found);
                    }

                    // Attach to single or multiple visits determined through
                    // the index
                } else {
                    Subject subject = Model.get().getSubject(encounterID.getSubjectKey());
                    if (subject != null) {
                        for (Visit visit : subject.getVisits()) {

                            // No mapping against artificial visits
                            if (visit.isArtificial()) {
                                continue;
                            }

                            // One to one
                            if (!config.isFindVisitViaTimestampOneToMany()) {

                                // Must match exactly
                                if (visit.getStart().compareTo(__start) <= 0 &&
                                    visit.getEnd().compareTo(__end) >= 0) {
                                    visits.add(visit);
                                    mappedByTimestamp++;
                                    break;
                                }

                                // One to many
                            } else {

                                // We take everything that overlaps
                                if ((visit.getStart().compareTo(__start) <= 0 &&
                                     visit.getEnd().compareTo(__start) >= 0) ||
                                    (visit.getStart().compareTo(__end) <= 0 &&
                                     visit.getEnd().compareTo(__end) >= 0)) {
                                    mappedByTimestamp++;
                                    visits.add(visit);
                                }
                            }
                        }
                    }
                }

                // Break, if no visit found
                if (visits.isEmpty()) {
                    continue;
                }

                // Collect data
                Data data = new Data(__start, __end);
                for (int j = 0; j < row.length; j++) {

                    // Extract
                    String attribute = header.get(j);
                    String value = row[j];

                    // Ignore columns not in the whitelist
                    if (attributes.getAttributes().contains(attribute)) {
                        data.put(attribute, value.trim());
                    }
                }

                // Data collection for summary: attach everything to the
                // artificial visit
                if (!Model.get().isI2b2() && Model.get().getConfig().isSummary()) {

                    // Attach to artificial visit
                    Subject subject = visits.iterator().next().getSubject();
                    if (subject != null) {
                        for (Visit visit : subject.getVisits()) {
                            if (visit.isArtificial()) {
                                visit.addData(config, data);
                            }
                        }
                    }

                    // Normal data collection
                } else {

                    // Attach to each visit found
                    for (Visit visit : visits) {
                        visit.addData(config, data);
                    }
                }
            }

            // Log
            LoggerUtil.info(" - Mapped by timestamp: " + mappedByTimestamp, this.getClass());
            LoggerUtil.info(" - Ignored due to missing visit: " + ignoredDueToMissingVisit,
                            this.getClass());
            LoggerUtil.info(" - Ignored due to missing timestamp: " + ignoredDueToMissingTimestamp,
                            this.getClass());
        }

        // Drop caches
        CSVReader.dropCaches();

        // Lock access by key
        Model.get().lockAccessByKey();

        // Done
        return RepeatStatus.FINISHED;
    }

    /**
     * Checks whether all columns to read are available in the input file.
     * Throws an IllegalArgumentException if columns not available in the input
     * file were configured.
     * 
     * @param header
     *            - columns which were read
     * @param columns
     *            - columns to read
     */
    private void checkHeader(Set<String> header, Set<String> columns, boolean blacklist) {

        // Find the column name which was not available in the input file, if
        // any.
        for (String column : columns) {
            if (!header.contains(column)) {
                String text = "Columns configured" + (blacklist ? " as blacklist:" : " to read:") +
                              columns.toString() + "\nComplete header read: " + header.toString() +
                              "\nColumn " + column + " not available";
                LoggerUtil.fatalUnchecked(text,
                                          ReadTaskletDataItem.class,
                                          IllegalArgumentException.class);
            }
        }
    }

    /**
     * Gets the indexes of the attributes to read. Returns the column names in
     * the input data file.
     * 
     * @param header
     * @param file
     * @throws IOException
     */
    private InputFileData getAttributeOffsets(List<String> header,
                                              ConfigurationInputFile file) throws IOException {

        // Prepare
        InputFileData result = new InputFileData();

        // Extract date format
        if (file.start != null) {
            result.startIndex = header.indexOf(file.start);
            result.endIndex = header.indexOf(file.end);
            if (file.dateFormat != null) {
                result.dateType = DateTimeFormatter.ofPattern(file.dateFormat);
            } else {
                LoggerUtil.fatalUnchecked("No date format specified",
                                          this.getClass(),
                                          IllegalArgumentException.class);
            }

            // Print
            LoggerUtil.info(" - Interpreting " + file.start + " as a start date with format " +
                            result.dateType,
                            this.getClass());
            if (file.end != null) {
                LoggerUtil.info(" - Interpreting " + file.end + " as an end date with format " +
                                result.dateType,
                                this.getClass());
            }

        } else if (file.hasSeveralColumnsForStartDate()) {

            result.indexStartDay = file.startDay != null ? header.indexOf(file.startDay) : -1;
            result.indexStartMonth = file.startMonth != null ? header.indexOf(file.startMonth) : -1;
            result.indexStartYear = file.startYear != null ? header.indexOf(file.startYear) : -1;

            if (file.hasSeveralColumnsForEndDate()) {
                result.indexEndDay = file.endDay != null ? header.indexOf(file.endDay) : -1;
                result.indexEndMonth = file.endMonth != null ? header.indexOf(file.endMonth) : -1;
                result.indexEndYear = file.endYear != null ? header.indexOf(file.endYear) : -1;
            }

            String field = "";
            field += file.startDay != null ? file.startDay : "";
            field += file.startMonth != null ? " " + file.startMonth : "";
            field += file.startYear != null ? " " + file.startYear : "";

            if (file.dateFormat != null) {
                result.dateType = DateTimeFormatter.ofPattern(file.dateFormat);
            } else {
                result.dateType = null;
            }
            LoggerUtil.info(" - Interpreting fields " + field + " as a date with format " +
                            result.dateType,
                            this.getClass());
        }

        // Done
        return result;
    }

    /**
     * Configure all information about attributes for this file
     * 
     * @param config
     * @return
     */
    private Entity getFileAttributeData(ConfigurationInputFile config) {

        // Obtain header
        Set<String> header = new HashSet<>(Arrays.asList(CSVReader.getHeader(config.getMapping(),
                                                                             config.file,
                                                                             config.charset)));

        // Attributes
        Set<String> attributes;

        // Whitelist or Blacklist
        if (!config.getWhitelist().isEmpty()) {
            // Whitelist
            attributes = new HashSet<>(config.getWhitelist());

        } else {
            // Blacklist
            attributes = new HashSet<>(header);

            // Check header for blacklist
            checkHeader(header, config.getBlacklist(), true);

            // Remove
            attributes.removeAll(config.getBlacklist());
        }

        // Determine all columns to read
        Set<String> columnsToRead = new HashSet<>(attributes);

        // Add the attribute for the single concept if configured.
        if (config.hasConceptSpecified()) {
            columnsToRead.add(config.concept);
        }

        // Add the attributes regarding start date and end date
        if (config.start != null) {
            columnsToRead.add(config.start);
        }
        if (config.startDay != null) {
            columnsToRead.add(config.startDay);
        }
        if (config.startMonth != null) {
            columnsToRead.add(config.startMonth);
        }
        if (config.startYear != null) {
            columnsToRead.add(config.startYear);
        }
        if (config.end != null) {
            columnsToRead.add(config.end);
        }
        if (config.endDay != null) {
            columnsToRead.add(config.endDay);
        }
        if (config.endMonth != null) {
            columnsToRead.add(config.endMonth);
        }
        if (config.endYear != null) {
            columnsToRead.add(config.endYear);
        }

        // Add key
        if (config.getKey() != null) {
            columnsToRead.addAll(config.getKey().getKeyAttributes());
        }

        // Determine concepts and modifiers
        HashSet<String> concepts = new HashSet<>();
        HashSet<String> modifiers = new HashSet<>();
        if (config.hasConceptSpecified()) {

            // We have just one concept
            concepts.add(config.concept);

            // TODO: Modifiers will be ignored at the moment (for transmart)
            if (Model.get().isI2b2()) {
                modifiers.addAll(attributes);
                modifiers.remove(config.concept);
            }
        } else {

            // All attributes are concepts
            concepts.addAll(attributes);
        }

        // Check header for all columns to read
        checkHeader(header, columnsToRead, false);

        // Pack
        return new Entity(attributes, columnsToRead, concepts, modifiers, config);
    }
}
