/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.difuture.components.i2b2transmart.model.Model;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

/**
 * Deletes an existing folder and recreates it.
 * 
 * @author Claudia Lang
 * 
 */
public class TaskletResetFolder implements Tasklet {

    @Override
    public RepeatStatus execute(StepContribution contribution,
                                ChunkContext chunkContext) throws Exception {
        // Folder
        String folderName;
        if (Model.get().isI2b2()) folderName = "/i2b2";
        else folderName = "/tranSMART";
        File folder = new File(Model.get().getConfig().getFolder().getAbsolutePath() + folderName);
        // Empty / create folder
        if (folder.exists()) {
            FileUtils.deleteDirectory(folder);
        }
        folder.mkdir();
        if (!Model.get().isI2b2()) folderName = "/tranSMART/clinical";
        folder = new File(Model.get().getConfig().getFolder().getAbsolutePath() + folderName);
        folder.mkdir();
        return RepeatStatus.FINISHED;
    }

}
