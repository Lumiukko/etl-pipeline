/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet.write;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.difuture.components.i2b2transmart.I2b2TableContents;
import org.difuture.components.i2b2transmart.configuration.ConfigurationFileFormat;
import org.difuture.components.i2b2transmart.configuration.ConfigurationSubject;
import org.difuture.components.i2b2transmart.model.Attribute;
import org.difuture.components.i2b2transmart.model.AttributeAge;
import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.i2b2transmart.model.Subject;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;

import com.carrotsearch.hppc.IntObjectOpenHashMap;

/**
 * Writes subject-data into a csv file confirming to I2b2 requirements.
 * 
 * @author Claudia Lang
 *
 */
public class WriteTaskletSubjectsI2b2 extends WriteTaskletSubjects {

    /**
     * Called by the spring batch framework.
     * 
     */
    @Override
    public RepeatStatus execute(StepContribution contribution,
                                ChunkContext chunkContext) throws Exception {
        super.execute(contribution, chunkContext);

        writeSubjectsAge();
        writeSubjectsGender();
        writeSubjectsVitalStatus();
        writeSubjectsRace();
        if (Model.get().getConfig().getSubjectConfiguration().loadIDs) {
            writeSubjectsID();
        }

        return RepeatStatus.FINISHED;
    }

    /**
     * Writes the subjects and their ages in a data file file for i2b2.
     * 
     */
    private void writeSubjectsAge() {
        ConfigurationSubject config = Model.get().getConfig().getSubjectConfiguration();

        // Output
        String fileName = "___subjects_age___.csv";

        // Header
        List<String> header = new ArrayList<>();
        header.add("___SUBJECT_ID___");
        // Start date is mandatory and used for the timeline in i2b2
        header.add("___START___");
        if (config.age != null) {
            for (int i = 0; i <= AttributeAge.MAX_VALUE; i++) {
                header.add(String.valueOf(i));
            }
            header.add(Attribute.UNKNOWN);

        }
        header.add(Attribute.NOT_RECORDED);

        // Prepare csv writer
        CSVWriter csvWriter = new CSVWriter(folder.getAbsolutePath() + "/" + fileName);
        Map<String, Integer> map = csvWriter.open(header.toArray(new String[header.size()]));

        // Store data about each subject
        for (Subject subject : Model.get().getSubjects()) {

            // Row
            IntObjectOpenHashMap<String> row = new IntObjectOpenHashMap<>();

            // Add subject id
            row.put(map.get("___SUBJECT_ID___"), subject.getId().toString());

            // Add start data
            row.put(map.get("___START___"),
                    subject.getDateFirstVisit()
                           .format(ConfigurationFileFormat.getInstance().getOutputDateFormatter()));

            // Add age
            row.put(map.get(subject.getAge().getValueAsString()),
                    subject.getAge().getValueAsString());

            // Write
            csvWriter.write(row);
        }

        // Close csv writer
        csvWriter.close();
    }

    /**
     * Creates the subjects and their gender in a data file file for i2b2.
     * 
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private void writeSubjectsGender() throws IOException {
        // Output
        String fileName = "___subjects_gender___.csv";

        // Header
        List<String> header = new ArrayList<>();
        header.add("___SUBJECT_ID___");
        // Start date is mandatory and used for the timeline in i2b2
        header.add("___START___");
        header.add(I2b2TableContents.CODE_LOOKUP_PAT_SEX.get("DEM|SEX:f"));
        header.add(I2b2TableContents.CODE_LOOKUP_PAT_SEX.get("DEM|SEX:m"));
        header.add(I2b2TableContents.CODE_LOOKUP_PAT_SEX.get("DEM|SEX:u"));
        header.add(I2b2TableContents.CODE_LOOKUP_PAT_SEX.get("DEM|SEX:@"));

        // Prepare csv writer
        CSVWriter csvWriter = new CSVWriter(folder.getAbsolutePath() + "/" + fileName);
        Map<String, Integer> map = csvWriter.open(header.toArray(new String[header.size()]));

        // Store data about each subject
        for (Subject subject : Model.get().getSubjects()) {

            // Row
            IntObjectOpenHashMap<String> row = new IntObjectOpenHashMap<>();

            // Data
            row.put(map.get("___SUBJECT_ID___"), subject.getId().toString());
            row.put(map.get("___START___"),
                    subject.getDateFirstVisit()
                           .format(ConfigurationFileFormat.getInstance().getOutputDateFormatter()));

            // Add sex
            if (subject.getSex() != null) {
                row.put(map.get(I2b2TableContents.CODE_LOOKUP_PAT_SEX.get(subject.getSex())),
                        subject.getSex());
            } else {
                row.put(map.get(I2b2TableContents.CODE_LOOKUP_PAT_SEX.get("DEM|SEX:@")),
                        "DEM|SEX:@");
            }

            // Write
            csvWriter.write(row);
        }

        // Close csv writer
        csvWriter.close();
    }

    /**
     * Writes the IDs for subjects for i2b2.
     * 
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private void writeSubjectsID() throws IOException {

        // Output
        String fileName = "___subjects_id___.csv";
        String prefix = "SYS|ID:";

        // Header
        List<String> header = new ArrayList<>();
        header.add("___SUBJECT_ID___");
        header.add("___START___");
        for (Subject subject : Model.get().getSubjects()) {
            String id = subject.getId().toString();
            header.add(prefix + id);
        }

        // Prepare csv writer
        CSVWriter csvWriter = new CSVWriter(folder.getAbsolutePath() + "/" + fileName);
        Map<String, Integer> map = csvWriter.open(header.toArray(new String[header.size()]));

        // Store data about each subject
        for (Subject subject : Model.get().getSubjects()) {

            // Row
            IntObjectOpenHashMap<String> row = new IntObjectOpenHashMap<>();

            // Data
            String id = subject.getId().toString();
            String code = prefix + id;
            row.put(map.get("___SUBJECT_ID___"), id);
            row.put(map.get("___START___"),
                    subject.getDateFirstVisit()
                           .format(ConfigurationFileFormat.getInstance().getOutputDateFormatter()));
            row.put(map.get(code), id);

            // Write
            csvWriter.write(row);
        }

        // Close csv writer
        csvWriter.close();
    }

    /**
     * Writes the subjects and their race in a data file file for i2b2.
     * 
     * So far only 'Not recorded' is supported. It is required for the breakdown
     * analyses.
     */
    private void writeSubjectsRace() {
        // Output
        String fileName = "___subjects_race___.csv";

        // Header
        List<String> header = new ArrayList<>();
        header.add("___SUBJECT_ID___");
        // Start date is mandatory and used for the timeline in i2b2
        header.add("___START___");
        header.add("Not recorded");
        // header.addAll(I2b2TableContents.CODE_LOOKUP_PAT_RACE.values());

        // Prepare csv writer
        CSVWriter csvWriter = new CSVWriter(folder.getAbsolutePath() + "/" + fileName);
        Map<String, Integer> map = csvWriter.open(header.toArray(new String[header.size()]));

        // Store data about each subject
        for (Subject subject : Model.get().getSubjects()) {

            // Row
            IntObjectOpenHashMap<String> row = new IntObjectOpenHashMap<>();

            // Data
            row.put(map.get("___SUBJECT_ID___"), subject.getId().toString());
            row.put(map.get("___START___"),
                    subject.getDateFirstVisit()
                           .format(ConfigurationFileFormat.getInstance().getOutputDateFormatter()));

            // Add race
            /*
             * if (subject.race != null) {
             * headersSelected.add(I2b2TableContents.CODE_LOOKUP_PAT_RACE.get(
             * subject.race)); data.add(subject.race); } else {
             * headersSelected.add(I2b2TableContents.CODE_LOOKUP_PAT_RACE.get(
             * "DEM|RACE:@")); data.add("DEM|RACE:@"); }
             */

            row.put(map.get("Not recorded"), "DEM|RACE:@");

            // Write
            csvWriter.write(row);
        }

        // Close csv writer
        csvWriter.close();
    }

    /**
     * Creates the subjects and their vital status in a data file file for i2b2.
     * 
     * So far only 'Not recorded' is supported.
     */
    private void writeSubjectsVitalStatus() {
        // Output
        String fileName = "___subjects_vital_status___.csv";

        // Header
        List<String> header = new ArrayList<>();
        header.add("___SUBJECT_ID___");
        // Start date is mandatory and used for the timeline in i2b2
        header.add("___START___");
        header.add("Not recorded");
        // header.addAll(I2b2TableContents.CODE_LOOKUP_PAT_VIT.values());

        // Prepare csv writer
        CSVWriter csvWriter = new CSVWriter(folder.getAbsolutePath() + "/" + fileName);
        Map<String, Integer> map = csvWriter.open(header.toArray(new String[header.size()]));

        // Store data about each subject
        for (Subject subject : Model.get().getSubjects()) {

            // Row
            IntObjectOpenHashMap<String> row = new IntObjectOpenHashMap<>();

            // Data
            row.put(map.get("___SUBJECT_ID___"), subject.getId().toString());
            row.put(map.get("___START___"),
                    subject.getDateFirstVisit()
                           .format(ConfigurationFileFormat.getInstance().getOutputDateFormatter()));

            // Add vital status
            /*
             * if (subject.vitalStatus != null) {
             * headersSelected.add(I2b2TableContents.CODE_LOOKUP_PAT_VIT.get(
             * subject.vitalStatus)); data.add(subject.vitalStatus); } else {
             * headersSelected.add(I2b2TableContents.CODE_LOOKUP_PAT_VIT.get(
             * "DEM|VITAL:@")); data.add("DEM|VITAL:@"); }
             */

            row.put(map.get("Not recorded"), "DEM|VITAL:@");

            // Write
            csvWriter.write(row);
        }

        // Close csv writer
        csvWriter.close();
    }
}
