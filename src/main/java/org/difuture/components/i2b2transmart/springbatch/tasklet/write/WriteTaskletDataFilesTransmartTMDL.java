/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet.write;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.difuture.components.i2b2transmart.configuration.ConfigurationFileFormat;
import org.difuture.components.i2b2transmart.configuration.ConfigurationInputFile;
import org.difuture.components.i2b2transmart.configuration.ConfigurationInputFileNumberFormat;
import org.difuture.components.i2b2transmart.model.Data;
import org.difuture.components.i2b2transmart.model.Entity;
import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.i2b2transmart.model.OntologyNodeConcept;
import org.difuture.components.i2b2transmart.model.Subject;
import org.difuture.components.i2b2transmart.model.Visit;
import org.difuture.components.util.FileUtil;
import org.difuture.components.util.LoggerUtil;

import com.carrotsearch.hppc.IntObjectOpenHashMap;

/**
 * Capsulates the transmart specific part, using loading tool
 * component-tm-dataloader
 * 
 * @author Claudia Lang
 *
 */
public class WriteTaskletDataFilesTransmartTMDL extends WriteTaskletDataFiles {

    /**
     * Returns the header for the staging file
     * 
     * @return a string array
     */
    private String[] getHeader(String observation, boolean useTimestamp) {
        List<String> result = new ArrayList<>();

        // Column 1 - Study ID
        result.add("STUDY_ID");
        // Column 2 - Identifying data
        result.add("SUBJ_ID");
        // Column 3 - Visit name
        result.add("VISIT_NAME");
        // Column 4 - Data label
        result.add("DATA_LABEL");
        if (useTimestamp) {
            // Column 5 - Baseline for timestamp
            result.add("Baseline");
            // Column 6 - Timepoint for timestamp
            result.add("TimePoint(TAG)");
        }

        result.add(observation);

        return result.toArray(new String[result.size()]);
    }

    /**
     * Writes the staging files for TranSMART
     */
    protected void write() {

        // TODO: necessary for tm?
        // Superroot - for ontology file for i2b2
        OntologyNodeConcept superRoot;
        Map<String, OntologyNodeConcept> fileConcept = new HashMap<String, OntologyNodeConcept>();

        // For each configuration input file
        for (ConfigurationInputFile config : Model.get().getConfig().getFiles()) {

            // Prepare
            ConfigurationInputFileNumberFormat numberConverter = config.getNumberConverter();
            Entity attributeData = Model.get().getEntity(config);

            // Extract concepts and modifiers
            String inputFileName = config.getName();
            if (!fileConcept.containsKey(inputFileName)) {
                superRoot = new OntologyNodeConcept(inputFileName, inputFileName);
                fileConcept.put(inputFileName, superRoot);
            } else {
                superRoot = fileConcept.get(inputFileName);
            }

            Set<String> concepts = attributeData.getConcepts();

            // For each concept
            for (String concept : concepts) {

                // Build list of attribute names to consider
                List<String> attributeNames = getAttributeNames(config, concept);

                // Study ID
                String studyID = Model.get().getConfig().getStudyID();

                // Prepare file name
                String fileName = FileUtil.escapeFileName(config, concept);
                if (Model.get().isUseTmDataLoader()) {
                    fileName = folder.getAbsolutePath() + "/" + studyID + "_" + fileName;
                } else {
                    fileName = folder.getAbsolutePath() + "/" + fileName;
                }

                // Prepare for header
                boolean useTimestamp = false;
                // TODO: Better. Problem is File5s.Mapping.ZNLMSVAKMM=MFI has
                // got no data
                if (attributeData.getDataType(concept) == null) {
                    break;
                }
                if (config.isTMDLUseTimestamp() &&
                    attributeData.getDataType(concept).equals("number") && !config.hasEncounter()) {
                    useTimestamp = true;
                }

                // Prepare csv writer
                CSVWriter csvWriter = new CSVWriter(fileName);
                csvWriter.open(getHeader(concept, useTimestamp));

                // Prepare log information
                int ignoredDueToMissingMapping = 0;
                long numberOfItemsWritten = 0;
                int ignoreVisitsWithoutSubject = 0;
                int ignoredVisitsDueToDuplicates = 0;
                long numberOfItemsTotal = 0;
                int ignoredDueToEmptyValues = 0;
                int ignoredDueToParseErrors = 0;

                // Status
                LoggerUtil.info("Start writing file: " + fileName, this.getClass());

                // For each data point available for this config
                for (Subject subject : Model.get().getSubjects()) {
                    for (Visit visit : subject.getVisits()) {

                        // Prepare
                        Set<Data> data = visit.getData(config);
                        numberOfItemsTotal++;

                        // Ignore empty values
                        if (data == null || data.isEmpty()) {
                            ignoredDueToEmptyValues++;
                            continue;
                        }

                        // For each observation
                        for (Data dataPoint : data) {

                            // Skip
                            if (dataPoint.isEmpty()) {
                                continue;
                            }

                            // Row to write
                            IntObjectOpenHashMap<String> row = new IntObjectOpenHashMap<>();

                            // Add Study ID
                            row.put(row.size(), studyID);

                            // Add basic info
                            row.put(row.size(), visit.getSubject().getId().toString());
                            row.put(row.size(), visit.getName());

                            // TODO: For transmart there are rows written,
                            // containing no observations
                            // due to 'continue;' commands in following for
                            // loop.
                            // Maybe this problem has already been solved in
                            // master?
                            // If not: find a better solution.
                            // Using tMDataLoader and Timepoints, these rows
                            // will lead to the exception:
                            // 'Check date format'
                            boolean writeDataPoint = false;

                            // For each observations (concept)
                            for (String observation : attributeNames) {

                                // Type
                                String attributeDataType = attributeData.getDataType(observation);

                                // If there is no data type, there is no data
                                // for this attribute
                                if (attributeDataType == null) {
                                    continue;
                                }

                                boolean isConceptTypeNumber = attributeDataType.equals("number");

                                // Get the value read for the i-th observations
                                String value = dataPoint.getValues().get(observation);

                                // Check if empty
                                if (isEmpty(value)) {
                                    continue;
                                }

                                writeDataPoint = true;

                                // Add label. Observation is used as label
                                row.put(row.size(), observation);

                                // Add tMDataLoader specific attributes -
                                // Timestamp
                                if (useTimestamp) {

                                    // Date first visit is used as baseline
                                    LocalDateTime baseline = subject.getDateFirstVisit()
                                                                    .getTimestamp();
                                    String baselineString = baseline.format(ConfigurationFileFormat.getInstance()
                                                                                                   .getOutputDateFormatter());
                                    row.put(row.size(), baselineString);

                                    // Start date is used as timepoint
                                    LocalDateTime timepoint = dataPoint.getStart().getTimestamp();
                                    String timepointString = timepoint.format(ConfigurationFileFormat.getInstance()
                                                                                                     .getOutputDateFormatter());
                                    row.put(row.size(), timepointString);
                                }

                                // Convert
                                String valueToWrite = value;
                                if (isConceptTypeNumber) {
                                    valueToWrite = numberConverter.convert(value);
                                    ignoredDueToParseErrors += valueToWrite == null ? 1 : 0;
                                    valueToWrite = valueToWrite == null ? "" : valueToWrite;
                                }

                                // Store
                                row.put(row.size(), valueToWrite);
                            }

                            // Write
                            if (writeDataPoint) {
                                csvWriter.write(row);
                                // Count
                                numberOfItemsWritten++;
                            }
                        }

                        // Count
                        numberOfItemsWritten++;
                    }
                }

                // Close csv writer
                csvWriter.close();

                // Log information
                LoggerUtil.info(" - End writing file: " + fileName, this.getClass());
                LoggerUtil.info(" - Items total: " + numberOfItemsTotal, this.getClass());
                LoggerUtil.info(" - Items written: " + numberOfItemsWritten, this.getClass());
                LoggerUtil.info(ignoredDueToEmptyValues,
                                " - Items ignored due to empty values: " + ignoredDueToEmptyValues,
                                this.getClass());
                LoggerUtil.info(ignoredDueToMissingMapping,
                                " - Items ignored due to missing mapping: " +
                                                            ignoredDueToMissingMapping,
                                this.getClass());
                LoggerUtil.info(ignoreVisitsWithoutSubject,
                                " - Items ignored due to missing subject: " +
                                                            ignoreVisitsWithoutSubject,
                                this.getClass());
                LoggerUtil.info(ignoredVisitsDueToDuplicates,
                                " - Items ignored due to duplicates: " +
                                                              ignoredVisitsDueToDuplicates,
                                this.getClass());
                LoggerUtil.info(ignoredDueToParseErrors,
                                " - Values ignored due to parsing exception: " +
                                                         ignoredDueToParseErrors,
                                this.getClass());
            }
        }
    }
}
