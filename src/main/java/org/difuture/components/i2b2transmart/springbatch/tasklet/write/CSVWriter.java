/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet.write;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.difuture.components.i2b2transmart.configuration.ConfigurationFileFormat;
import org.difuture.components.util.LoggerUtil;

import com.carrotsearch.hppc.IntObjectOpenHashMap;
import com.univocity.parsers.csv.CsvWriter;
import com.univocity.parsers.csv.CsvWriterSettings;

/**
 * Utility class for writing the staging files.
 * 
 * @author Claudia Lang
 * @author Fabian Prasser
 */
class CSVWriter {

    /** Writer */
    private CsvWriter                         writer;

    /** Cache */
    private Set<IntObjectOpenHashMap<String>> cache   = null;

    /** Number of columns */
    private int                               columns = -1;

    /**
     * Creates the csv writer. Initializes the settings for the csv writer
     * 
     * @param fileName
     *            - the absolute name of the csv file
     */
    CSVWriter(String fileName) {
        this(fileName, false);
    }

    /**
     * Creates the csv writer. Initializes the settings for the csv writer
     * 
     * @param fileName
     *            - the absolute name of the csv file
     * @param deduplicate
     */
    CSVWriter(String fileName, boolean deduplicate) {
        // The settings for the parser
        CsvWriterSettings settings = new CsvWriterSettings();
        settings.setEmptyValue("");
        settings.setNullValue("");
        settings.setQuoteAllFields(false);
        settings.getFormat().setLineSeparator(ConfigurationFileFormat.getInstance().getNewLine());
        settings.getFormat()
                .setDelimiter(ConfigurationFileFormat.getInstance().getDelimiterOutput());
        writer = new CsvWriter(new File(fileName), settings);
        if (deduplicate) {
            cache = new HashSet<IntObjectOpenHashMap<String>>();
        }
    }

    /**
     * Writes a single row
     * 
     * @param row
     */
    public void write(String[] row) {

        // Check
        if (columns == -1) { throw new IllegalStateException("File has not been opened, yet."); }

        // Convert
        IntObjectOpenHashMap<String> map = new IntObjectOpenHashMap<>();
        for (int column = 0; column < Math.min(columns, row.length); column++) {
            map.put(column, row[column]);
        }

        // Write
        this.write(map);
    }

    /**
     * Closes the csv writer
     */
    void close() {
        writer.close();
    }

    /**
     * Open incl. header
     * 
     * @param header
     * @return
     */
    Map<String, Integer> open(String[] header) {
        writer.writeHeaders(header);
        columns = header.length;
        Map<String, Integer> map = new HashMap<>();
        for (int i = 0; i < header.length; i++) {
            map.put(header[i], i);
        }
        return map;
    }

    /**
     * Writes a complete single row
     * 
     * @param data
     *            - a string array
     */
    void write(IntObjectOpenHashMap<String> data) {

        // Check
        if (columns == -1) { throw new IllegalStateException("File has not been opened, yet."); }

        // If not cached
        if (cache == null || cache.add(data)) {

            // Write row
            String[] row = new String[columns];
            for (int column = 0; column < columns; column++) {
                row[column] = data.getOrDefault(column, null);
            }
            writer.writeRow(row);

            // Warn
        } else {
            LoggerUtil.warn("[BUG] Deduplication occured at file level", this.getClass());
        }
    }
}
