/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet.write;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.difuture.components.i2b2transmart.configuration.ConfigurationInputFile;
import org.difuture.components.i2b2transmart.configuration.ConfigurationSubject;
import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.i2b2transmart.model.OntologyNodeConcept;
import org.difuture.components.util.FileUtil;

/**
 * Writes the mapping file for tMDataLoader
 * 
 * @author Claudia Lang
 *
 */
public class WriteTaskletMappingFileTransmartTMDL extends WriteTaskletMappingFile {

    /** Number of the label column */
    private final int columnNumberLabel = 4;

    /**
     * Escapes characters for the transmart mapping file
     * 
     * @param entityName
     * @return
     */
    private String escape(String value) {
        return value.replace("+", "\\+");
    }

    /**
     * Gets the entries for the concepts to the mapping file.
     */
    private List<String[]> getEntriesForConceptNodes() {
        List<String[]> result = new ArrayList<>();

        // Pos 0: subject id
        // Pos 1: visit name
        // Pos 2: label
        // Pos 3: value
        // Note: The indexes for the columns start in component transmart batch
        // at 1 instead of 0
        int indexMappingFileMod = 5;

        // For each input file and concept
        for (ConfigurationInputFile config : Model.get().getConfig().getFiles()) {

            Map<String, OntologyNodeConcept> conceptNodeHierarchie = Model.get()
                                                                          .getOntology()
                                                                          .getOntologiesForConcepts(config);

            for (String concept : Model.get().getEntity(config).getConcepts()) {
                String fileName = FileUtil.escapeFileName(config, concept);
                OntologyNodeConcept conceptNode = conceptNodeHierarchie.get(concept);

                // If there is no concept node, there is no data
                if (conceptNode == null) {
                    continue;
                }

                boolean isNumber = conceptNode.getDataType().equals("N");
                boolean useTimestamp = config.isTMDLUseTimestamp() && !config.hasEncounter() &&
                                       isNumber;
                boolean useVisitname;

                if (useTimestamp) {
                    useVisitname = false;
                } else {
                    useVisitname = config.hasEncounter() || config.isFindVisitViaTimestamp();
                }

                String eavName = config.isEAV() ? config.getEAV().getAttribute() : null;
                result.addAll(getEntriesInMappingFilePlaceholderValues(fileName,
                                                                       useTimestamp,
                                                                       useVisitname));
                if (useTimestamp) {
                    indexMappingFileMod = 7;
                } else {
                    indexMappingFileMod = 5;
                }
                result.add(getEntriesInMappingFileEntity(fileName,
                                                         indexMappingFileMod,
                                                         config.getName(),
                                                         config.path,
                                                         concept,
                                                         isNumber ? "Numerical" : "Categorical",
                                                         useVisitname,
                                                         eavName,
                                                         useTimestamp));
            }
        }

        return result;
    }

    /**
     * Creates a new line to add to the file mapping.params
     * 
     * @param fileName
     *            - the name of the output file
     * @param columnNumber
     *            - the position of the attribute, starting at 1
     * @param entityName
     *            - the name of the entity
     * @param path
     *            - a path to add in the hiearchie
     * @param attribute
     *            - the attribute to write (concept)
     * @param nodeType
     *            - the type of the attribute (numerical or categorical)
     * @param recordsWithVisit
     *            - if there is visit and its name is to use, place holder
     *            VISITNAME is to write
     * @param eavName
     *            - the name of the eav entity, if this entity is a generic one
     * @param useTimestamp
     *            - if the timestamp to use for this concept, place holder
     *            $$TIME is to use. the timestamp will be shown as concept in
     *            tranSMART
     * 
     * @return a string array
     */
    private String[] getEntriesInMappingFileEntity(String fileName,
                                                   int columnNumber,
                                                   String entityName,
                                                   String path,
                                                   String attribute,
                                                   String nodeType,
                                                   boolean recordsWithVisit,
                                                   String eavName,
                                                   boolean useTimestamp) {

        String categoryCode = "Characteristics";

        if (!recordsWithVisit) {
            if (path != null) {
                categoryCode = categoryCode + "+" + path;
            }

            if (eavName != null) {
                categoryCode = categoryCode + "+" + escape(eavName);
            }
            categoryCode += "+" + escape(entityName);

        } else {

            if (path != null) {
                categoryCode = categoryCode + "+" + path;
            }

            if (eavName != null) {
                categoryCode = categoryCode + "+" + escape(eavName);
            }

            categoryCode = categoryCode + "+VISITNAME+" + escape(entityName);
        }

        categoryCode = categoryCode + "+DATALABEL";

        if (useTimestamp) {
            categoryCode = categoryCode + "+$$TIME";
            return getRow(fileName,
                          categoryCode,
                          columnNumber,
                          "\\",
                          String.valueOf(columnNumberLabel),
                          "Baseline",
                          "Timestamp",
                          "");
        }

        // tMDataLoader detects data type automatically, but wrong, if there are
        // numericals and text mixed in one observation fact
        // and there are visits only containing numericals.
        // return getRow(fileName, categoryCode, columnNumber, "\\\\",
        // String.valueOf(columnNumberLabel), "", nodeType.equals("Categorical")
        // ? nodeType : "", "");
        return getRow(fileName,
                      categoryCode,
                      columnNumber,
                      "\\",
                      String.valueOf(columnNumberLabel),
                      "",
                      "",
                      "");
    }

    /**
     * Gets the entries for the placeholders in the mapping file. Place holders
     * are all but the observation data. Examples for place holders: - Study ID
     * - Subject ID - Visit Name - Label - Baseline - Time
     * 
     * @param fileName
     */
    private List<String[]> getEntriesInMappingFilePlaceholderValues(String fileName,
                                                                    boolean useTimestamp,
                                                                    boolean useVisitname) {
        List<String[]> result = new ArrayList<>();
        int column = 1;

        // Study ID
        result.add(getRow(fileName, "", column++, "STUDY_ID", null, null, null, null));

        // Subject ID
        result.add(getRow(fileName, "", column++, "SUBJ_ID", null, null, null, null));

        // Visit name - OMIT means to ignore visit name
        String visitName = useVisitname ? "VISIT_NAME" : "OMIT";
        result.add(getRow(fileName, "", column++, visitName, null, null, null, null));

        // Data Label
        result.add(getRow(fileName,
                          "Characteristics",
                          column++,
                          "DATA_LABEL",
                          null,
                          null,
                          null,
                          null));

        if (useTimestamp) {
            result.add(getRow(fileName, "", column++, "Baseline", null, null, null, null));
            result.add(getRow(fileName, "", column++, "TIME", null, null, null, null));
        }

        return result;
    }

    /**
     * Gets the entries for the subjects in the mapping file
     * 
     * @param mapping
     */
    private List<String[]> getEntriesInMappingFileSubjects() {
        List<String[]> result = new ArrayList<>();

        // Prepare
        ConfigurationSubject config = Model.get().getConfig().getSubjectConfiguration();
        String pathSubject = "Characteristics+" + config.path;
        String fileName = "subjects.txt";
        int column = 1;

        result.add(getRow(fileName, "", column++, "STUDY_ID", "", "", "", ""));
        result.add(getRow(fileName, "", column++, "SUBJ_ID", "", "", "", ""));

        // Write columns
        if (config.sex != null) {
            result.add(getRow(fileName, pathSubject, column++, "Sex", "", "", "", ""));
        }
        if (config.zip != null) {
            result.add(getRow(fileName, pathSubject, column++, config.zip, "", "", "", ""));
        }
        if (config.age != null) {
            result.add(getRow(fileName, pathSubject, column++, "Age", "", "", "", ""));
        }
        if (config.birthYear != null) {
            result.add(getRow(fileName, pathSubject, column++, config.birthYear, "", "", "", ""));
        }
        if (config.birthMonth != null) {
            result.add(getRow(fileName, pathSubject, column++, config.birthMonth, "", "", "", ""));
        }
        if (config.birthDate != null) {
            result.add(getRow(fileName, pathSubject, column++, config.birthDate, "", "", "", ""));
        }
        if (config.loadIDs) {
            result.add(getRow(fileName, pathSubject, column++, "ID", "", "", "", ""));
        }

        return result;
    }

    /**
     * Gets the row to write in the mapping file.
     * 
     * @param filename
     * @param category
     * @param colNbr
     * @param label
     * @param labelSource
     * @param baseline
     * @param type
     * @param validation
     * @return
     */
    private String[] getRow(String filename,
                            String category,
                            int colNbr,
                            String label,
                            String labelSource,
                            String baseline,
                            String type,
                            String validation) {
        List<String> result = new ArrayList<>();
        String prefix = Model.get().getConfig().getStudyID();
        result.add(prefix + "_" + filename);
        result.add(category);
        result.add(String.valueOf(colNbr));
        result.add(label);
        result.add(labelSource);
        result.add(baseline);
        result.add(type);
        result.add(validation);

        return result.toArray(new String[result.size()]);
    }

    /**
     * Gets all rows to write in the mapping file
     * 
     * @return a list of string arrays
     */
    @Override
    protected List<String[]> getEntriesToMappingFile() {
        List<String[]> result = new ArrayList<>();

        result.addAll(getEntriesForConceptNodes());
        result.addAll(getEntriesInMappingFileSubjects());

        return result;
    }

    /**
     * Returns the header for the mapping file
     * 
     * @return a string array
     */
    @Override
    protected String[] getHeader() {
        return new String[] { "filename",
                              "category_code",
                              "col_nbr",
                              "data_label",
                              "data_label_source",
                              "baseline",
                              "variable_type",
                              "validation_rules" };

    }

}
