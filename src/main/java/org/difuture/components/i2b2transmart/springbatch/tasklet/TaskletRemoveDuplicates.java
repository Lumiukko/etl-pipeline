/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.springbatch.tasklet;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.difuture.components.i2b2transmart.configuration.ConfigurationInputFile;
import org.difuture.components.i2b2transmart.configuration.ConfigurationInputFileNumberFormat;
import org.difuture.components.i2b2transmart.model.Data;
import org.difuture.components.i2b2transmart.model.Entity;
import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.i2b2transmart.model.Subject;
import org.difuture.components.i2b2transmart.model.Timestamp;
import org.difuture.components.i2b2transmart.model.Visit;
import org.difuture.components.util.LoggerUtil;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

/**
 * Checks for each entity and concept, whether duplicates are allowed and if
 * not, removes duplicates. A duplicate may occur for example if a patient gets
 * several diagnoses in the same visit.
 * 
 * If a concept has a numerical type, a duplication will be detected if the
 * observation has the same subject id, the same visit id and if the staging
 * files are for i2b2 the start and end date will be considered additionally. If
 * a concept is non numerical, the value of the concept will be used
 * additionally.
 * 
 * "Modifier" and "concept" are sub-categories of "observation"
 * 
 * @author Claudia Lang
 * @author Fabian Prasser
 */
public class TaskletRemoveDuplicates implements Tasklet {

    /**
     * Class value wrapper
     * 
     * @author Fabian Prasser
     */
    private static class I2b2DataPoint {

        /** Date time */
        private Timestamp start;
        /** Value */
        private String    value;

        /**
         * Create a new instance
         * 
         * @param start
         * @param end
         * @param value
         */
        I2b2DataPoint(Timestamp start, String value) {
            this.start = start;
            this.value = value;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) return true;
            if (obj == null) return false;
            if (getClass() != obj.getClass()) return false;
            I2b2DataPoint other = (I2b2DataPoint) obj;
            if (start == null) {
                if (other.start != null) return false;
            } else if (!start.equals(other.start)) return false;
            if (value == null) {
                if (other.value != null) return false;
            } else if (!value.equals(other.value)) return false;
            return true;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((start == null) ? 0 : start.hashCode());
            result = prime * result + ((value == null) ? 0 : value.hashCode());
            return result;
        }
    }

    /** String */
    public static final String LATEST  = " - Latest";
    /** String */
    public static final String OLDEST  = " - Oldest";
    /** String */
    public static final String MINIMUM = " - Minimum";
    /** String */
    public static final String MAXIMUM = " - Maximum";
    /** String */
    public static final String MEAN    = " - Mean";

    /** String */
    public static final String SET     = " - Set";

    @Override
    public RepeatStatus execute(StepContribution stepContribution,
                                ChunkContext chunkContext) throws Exception {

        // Data collection for summary: attach everything to the artificial
        // visit
        if (!Model.get().isI2b2() && Model.get().getConfig().isSummary()) {
            removeSummary();
        } else {
            removeStandard();
        }

        // Done
        return RepeatStatus.FINISHED;
    }

    /**
     * Standard way of removing data
     */
    private void removeStandard() {

        // Log
        LoggerUtil.info("Removing duplicate entries", this.getClass());

        // For each input file
        for (ConfigurationInputFile config : Model.get().getConfig().getFiles()) {

            // Prepare
            int ignored = 0;
            Entity attributes = Model.get().getEntity(config);
            boolean hasMultipleConcepts = attributes.getConcepts().size() > 1;

            // For each data point available for this config
            for (Subject subject : Model.get().getSubjects()) {
                for (Visit visit : subject.getVisits()) {

                    // Prepare
                    Set<Data> data = visit.getData(config);

                    // Skip
                    if (data == null || data.isEmpty()) {
                        continue;
                    }

                    // For each concept
                    for (String concept : attributes.getConcepts()) {

                        // Remove all elements but the first one for numeric
                        // attributes
                        // TODO: Can we have multiple numeric entries with
                        // differing timestamps in i2b2?
                        if (attributes.isAttributeDataTypeNumber(concept)) {

                            // If has further concepts: remove only values for
                            // this concept
                            if (hasMultipleConcepts) {

                                Iterator<Data> iterator = data.iterator();
                                iterator.next(); // Preserve first
                                while (iterator.hasNext()) {
                                    iterator.next().getValues().put(concept, ""); // Remove
                                                                                  // all
                                                                                  // other
                                                                                  // entries
                                    ignored++;
                                }

                                // Else (either has modifiers, or just one
                                // concept): remove complete entries
                            } else {

                                Iterator<Data> iterator = data.iterator();
                                iterator.next(); // Preserve first
                                while (iterator.hasNext()) {
                                    Data dataPoint = iterator.next();
                                    iterator.remove(); // Remove all other
                                                       // entries
                                    ignored += dataPoint.getValues().size();
                                }
                            }

                        } else {

                            // If i2b2, also take a look at the timestamp
                            if (Model.get().isI2b2()) {

                                Set<I2b2DataPoint> values = new HashSet<>();
                                Iterator<Data> iterator = data.iterator();
                                while (iterator.hasNext()) {

                                    Data _data = iterator.next();
                                    String value = _data.getValues().get(concept);
                                    Timestamp start = _data.getStart();
                                    I2b2DataPoint dataPoint = new I2b2DataPoint(start, value);

                                    // Detect
                                    if (!values.contains(dataPoint)) {
                                        values.add(dataPoint);
                                    } else if (hasMultipleConcepts) {
                                        _data.getValues().put(concept, ""); // Remove
                                                                            // specific
                                                                            // value
                                        ignored++;
                                    } else {
                                        iterator.remove();
                                        ignored += _data.getValues().size();
                                    }
                                }

                                // Just remove duplicate values
                            } else {

                                Set<String> values = new HashSet<>();
                                Iterator<Data> iterator = data.iterator();
                                while (iterator.hasNext()) {

                                    Data dataPoint = iterator.next();
                                    String value = dataPoint.getValues().get(concept);
                                    if (!values.contains(value)) {
                                        values.add(value);
                                    } else if (hasMultipleConcepts) {
                                        dataPoint.getValues().put(concept, ""); // Remove
                                                                                // specific
                                                                                // value
                                        ignored++;
                                    } else {
                                        iterator.remove();
                                        ignored += dataPoint.getValues().size();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            LoggerUtil.info(ignored,
                            " - Entity " + config.getName() + ": ignored " + ignored +
                                     " values due to duplication",
                            this.getClass());

            // Debugging purposes: perform additional duplicate removal step
            // that should not be necessary
            int warningIgnored = 0;

            // For each data point available for this config
            for (Subject subject : Model.get().getSubjects()) {
                for (Visit visit : subject.getVisits()) {

                    // Prepare
                    Set<Data> data = visit.getData(config);

                    // Skip
                    if (data == null || data.isEmpty()) {
                        continue;
                    }

                    // Scan for complete duplicates
                    Set<Data> set = new HashSet<>();
                    Iterator<Data> iterator = data.iterator();
                    while (iterator.hasNext()) {
                        if (!set.add(iterator.next())) {
                            iterator.remove();
                            warningIgnored++;
                        }
                    }
                }
            }

            // Print warning
            LoggerUtil.warn(warningIgnored,
                            " - Entity " + config.getName() + ": [BUG] Ignored " + warningIgnored +
                                            " duplicate values that should not be present",
                            this.getClass());
        }
    }

    /**
     * Removing data for summaries
     */
    private void removeSummary() {

        // Log
        LoggerUtil.info("Compiling summary", this.getClass());

        // For each input file
        for (ConfigurationInputFile config : Model.get().getConfig().getFiles()) {

            // Prepare
            Entity attributes = Model.get().getEntity(config);

            // For each data point available for this config
            for (Subject subject : Model.get().getSubjects()) {

                // For each visit
                for (Visit visit : subject.getVisits()) {

                    // Prepare
                    Set<Data> data = visit.getData(config);

                    // Assumption: there is only one (artificial) visit, for
                    // each subject
                    if (data == null || data.isEmpty()) {
                        continue;
                    }

                    // The one data object to remain
                    Data noSummary = new Data();
                    Data summary = new Data();
                    Set<Data> categories = new HashSet<>();

                    // For each concept
                    for (String concept : attributes.getConcepts()) {

                        // Specific parameters for numbers
                        String min = null;
                        Number tMin = null;
                        String max = null;
                        Number tMax = null;
                        Number tMean = null;
                        long count = 0;

                        // General parameters
                        Timestamp tLatest = null;
                        Timestamp tOldest = null;
                        String latest = null;
                        String oldest = null;

                        // For all available data
                        for (Data entries : data) {

                            // Skip
                            if (!entries.getValues().containsKey(concept)) {
                                continue;
                            }

                            // Latest
                            if (tLatest == null || entries.getEnd().isAfter(tLatest)) {
                                tLatest = entries.getEnd();
                                latest = entries.getValues().get(concept);
                            }

                            // Oldest
                            if (tOldest == null || entries.getStart().isBefore(tOldest)) {
                                tOldest = entries.getStart();
                                oldest = entries.getValues().get(concept);
                            }
                        }

                        // Store
                        if (latest != null) {
                            summary.put(concept + LATEST, latest);
                            noSummary.put(concept, latest);
                        }
                        if (oldest != null) {
                            summary.put(concept + OLDEST, oldest);
                        }

                        // Specifically for numbers
                        if (attributes.isAttributeDataTypeNumber(concept)) {

                            // Converter
                            ConfigurationInputFileNumberFormat converter = config.getNumberConverter();

                            // For all values
                            for (Data entries : data) {

                                // Skip
                                if (!entries.getValues().containsKey(concept)) {
                                    continue;
                                }

                                // Read
                                String value = entries.getValues().get(concept);
                                Number number = converter.getNumber(value);

                                // Skip
                                if (number == null) {
                                    continue;
                                }

                                // Min
                                if (tMin == null || tMin.doubleValue() > number.doubleValue()) {
                                    tMin = number;
                                    min = value;
                                }
                                // Max
                                if (tMax == null || tMax.doubleValue() < number.doubleValue()) {
                                    tMax = number;
                                    max = value;
                                }
                                // Mean
                                if (tMean == null) {
                                    tMean = number;
                                    count++;
                                } else {
                                    tMean = tMean.doubleValue() + number.doubleValue();
                                    count++;
                                }
                            }

                            // Store
                            if (tMean != null) {
                                tMean = tMean.doubleValue() / (double) count;
                                summary.put(concept + MEAN, converter.toString(tMean));
                            }
                            if (min != null) {
                                summary.put(concept + MINIMUM, min);
                            }
                            if (max != null) {
                                summary.put(concept + MAXIMUM, max);
                            }
                        } else {

                            // For all values
                            Set<String> values = new HashSet<>();
                            for (Data entries : data) {

                                // Skip
                                if (!entries.getValues().containsKey(concept)) {
                                    continue;
                                }

                                // Add categorical
                                values.add(entries.getValues().get(concept));
                            }

                            // Add distinct
                            for (String distinct : values) {
                                Data categoryData = new Data();
                                categoryData.put(concept + SET, distinct);
                                categories.add(categoryData);
                            }
                        }
                    }

                    // Update
                    data.clear();

                    if (config.noSummary) {
                        data.add(noSummary);
                    } else {
                        data.add(summary);
                        data.addAll(categories);
                    }
                }
            }
            LoggerUtil.info("Summary created", this.getClass());
        }
    }
}
