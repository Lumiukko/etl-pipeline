/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.SQLException;

import org.apache.commons.io.FileUtils;
import org.difuture.components.i2b2transmart.configuration.Configuration;
import org.difuture.components.i2b2transmart.configuration.ConfigurationFileFormat;
import org.difuture.components.i2b2transmart.model.Model;
import org.difuture.components.util.LoggerUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

/**
 * Main entry point for staging with Spring Batch
 * 
 * @author Claudia Lang
 * @author Fabian Prasser
 *
 */
@ComponentScan
@EnableAutoConfiguration
public class ETL {

    public static class Args {
        @Parameter(names = { "-m", "--mode" }, description = "Working mode", required = true)
        public String mode;

        @Parameter(names = { "-f",
                             "--folder" }, description = "Folder containing configuration.properties", required = true)
        public String folder;

        @Parameter(names = { "-t",
                             "--target" }, description = "Target, either tm for transmart or i2b2", required = true)
        public String target;

        @Parameter(names = { "-c", "--charset" }, description = "Charset")
        public String charset = "default";

        @Parameter(names = { "-maxcols",
                             "--columnread" }, description = "Maximal columns supported in input files")
        public String colread = "default";

        @Parameter(names = { "-h", "--host" }, description = "Host of the target database")
        public String host;

        @Parameter(names = { "-prt", "--port" }, description = "Port of the target database")
        public String port;

        @Parameter(names = { "-i", "--instance" }, description = "Instance of the target database")
        public String instance;

        @Parameter(names = { "-u", "--user" }, description = "User name of the target database")
        public String user;

        @Parameter(names = { "-p", "--password" }, description = "Password of the target database")
        public String password;

        @Parameter(names = { "-l", "--loadingtool" }, description = "Name of the tool to load data")
        public String loadingTool;

        @Parameter(names = { "-s",
                             "--secret" }, description = "Cient secret for accessing the pseudonymization service")
        public String clientSecret;
    }

    public static void
           main(String[] argv) throws ClassNotFoundException, IOException, SQLException {
        Args args = new Args();
        JCommander.newBuilder().addObject(args).build().parse(argv);

        // The charset to use
        Charset charset = args.charset.equals("default") ? Charset.defaultCharset()
                : Charset.forName(args.charset);
        Integer maxColRead = args.colread.equals("default") ? 1024 : Integer.parseInt(args.colread);
        // Open
        File folder = new File(args.folder);

        // Check
        if (!folder.exists()) {
            LoggerUtil.fatalUnchecked("Specified file is not a folder",
                                      ETL.class,
                                      IllegalArgumentException.class);
        } else if (!folder.isDirectory()) {
            LoggerUtil.fatalUnchecked("Please specify a folder",
                                      ETL.class,
                                      IllegalArgumentException.class);
        }

        if (args.target.equals("i2b2")) {
            Model.get().setI2b2(true);
        } else {
            Model.get().setI2b2(false);
        }

        if (args.loadingTool != null && args.loadingTool.equals("tmdl")) {
            if (Model.get().isI2b2()) {
                LoggerUtil.fatalUnchecked("The tool tMDataLoader is to use with TranSMART",
                                          ETL.class,
                                          IllegalArgumentException.class);
            }
            Model.get().setTmDataLoader(true);
        } else {
            Model.get().setTmDataLoader(false);
        }

        // Load config
        Configuration config = new Configuration(folder, false, args);
        ConfigurationFileFormat.getInstance().setCharset(charset);
        ConfigurationFileFormat.getInstance().setMaxcolsread(maxColRead);
        Model.get().setConfig(config);

        if ("stage".equals(args.mode)) {
            // Start Spring Batch
            SpringApplication.run(ETL.class, argv);

        } else if ("load".equals(args.mode)) {
            // Load data to DWH
            if (args.target.equals("tm")) {
                TranSMARTOperations tranSMARTOperations = new TranSMARTOperations(config);
                if (Model.get().isUseTmDataLoader()) {
                    // TODO: New Argument
                    tranSMARTOperations.prepareForTmDataLoader();
                }
                // Perform loading
                tranSMARTOperations.load();
            } else if (args.target.equals("i2b2")) {
                I2b2Operations i2b2Operations = new I2b2Operations(config);
                // Perform loading
                i2b2Operations.prepare();
                i2b2Operations.load();

            } else {
                LoggerUtil.fatalUnchecked("Invalid target: " + args.target,
                                          ETL.class,
                                          IllegalArgumentException.class);
            }

        } else if ("unstage".equals(args.mode)) {
            // Remove staged files
            if (args.target.equals("i2b2")) {
                // Folder
                folder = new File(config.getFolder().getAbsolutePath() + "/i2b2");
            } else {
                // Folder
                folder = new File(config.getFolder().getAbsolutePath() + "/tranSMART");
            }
            // Empty/create folder
            if (folder.exists()) {
                FileUtils.deleteDirectory(folder);
            }
            folder.mkdir();

        } else if ("delete".equals(args.mode)) {
            // Delete data from DWH

            if (args.target.equals("tm")) {
                TranSMARTOperations tranSMARTOperations = new TranSMARTOperations(config);
                // Delete
                tranSMARTOperations.delete();
            } else if (args.target.equals("i2b2")) {
                I2b2Operations i2b2Operations = new I2b2Operations(config);
                // Delete
                i2b2Operations.delete();
            } else {
                LoggerUtil.fatalUnchecked("Invalid target: " + args.target,
                                          ETL.class,
                                          IllegalArgumentException.class);
            }

        } else {
            LoggerUtil.fatalUnchecked("Invalid mode: " + args.mode +
                                      ". Valid modes are stage, unstage, load, delete",
                                      ETL.class,
                                      IllegalArgumentException.class);
        }
    }
}
