/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.difuture.components.i2b2transmart.configuration.ConfigurationEncounter;
import org.difuture.components.i2b2transmart.configuration.ConfigurationFileFormat;
import org.difuture.components.i2b2transmart.configuration.ConfigurationInputFile;

/**
 * Model class for visits
 * 
 * @author Fabian Prasser
 */
public class Visit {

    /** The name for artificial visits. */
    protected static String                        NAME_ARTIFICIAL_VISIT = "N/A";

    /** Visit name */
    private String                                 name;
    /** Visit date. Beginning of visit. */
    private Timestamp                              start;
    /** Visit date. End of visit. */
    private Timestamp                              end;
    /** activeStatus */
    private String                                 activeStatus;
    /** inout */
    private String                                 inout;
    /** location */
    private String                                 location;
    /** location path */
    private String                                 locationPath;
    /** length of stay */
    private Integer                                lengthOfStay;
    /** blob */
    private String                                 blob;
    /** Patient */
    private Subject                                subject;
    /** Encounter id */
    private Key                                    id;

    /** Data */
    private Map<ConfigurationInputFile, Set<Data>> data                  = new HashMap<>();

    /**
     * Creates an artificial visit.
     * 
     * @param subjectID
     */
    Visit(Key subjectID) {
        this(subjectID.getArtificalVisitKey(),
             NAME_ARTIFICIAL_VISIT,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null);
        this.subject = Model.get().getSubject(subjectID);
    }

    /**
     * Creates a new instance for i2b2. Visit will be registered with the list
     * of visits.
     * 
     * @param id
     * @param name
     * @param start
     * @param end
     * @param activeStatus
     * @param inout
     * @param location
     * @param locationPath
     * @param lengthOfStay
     * @param blob
     */
    Visit(Key id,
          String name,
          Timestamp start,
          Timestamp end,
          String activeStatus,
          String inout,
          String location,
          String locationPath,
          Integer lengthOfStay,
          String blob) {
        this.id = id;
        this.name = name;
        this.start = start;
        this.end = end;
        this.activeStatus = activeStatus;
        this.inout = inout;
        this.location = location;
        this.locationPath = locationPath;
        this.lengthOfStay = lengthOfStay;
        this.blob = blob;
    }

    /**
     * Adds data
     * 
     * @param config
     * @param data
     * @return
     */
    public void addData(ConfigurationInputFile config, Data data) {
        Set<Data> set = this.data.get(config);
        if (set == null) {
            set = new HashSet<Data>();
            this.data.put(config, set);
        }
        set.add(data);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Visit other = (Visit) obj;
        return this.id.equals(other.id);
    }

    /**
     * Getter
     * 
     * @return
     */
    public String getActiveStatus() {
        return activeStatus;
    }

    /**
     * Returns all data, independent of the associated entity
     * 
     * @return
     */
    public Collection<Data> getAllData() {
        Set<Data> data = new HashSet<>();
        for (Set<Data> other : this.data.values()) {
            data.addAll(other);
        }
        return data;
    }

    /**
     * Getter
     * 
     * @return
     */
    public String getBlob() {
        return blob;
    }

    /**
     * Returns the map
     * 
     * @return
     */
    public Map<ConfigurationInputFile, Set<Data>> getData() {
        return this.data;
    }

    /**
     * Returns data for this config
     * 
     * @param config
     * @return
     */
    public Set<Data> getData(ConfigurationInputFile config) {
        return this.data.get(config);
    }

    /**
     * Getter
     * 
     * @return
     */
    public Timestamp getEnd() {
        return end;
    }

    /**
     * Returns the visit id
     * 
     * @return
     */
    public Key getId() {
        return this.id;
    }

    /**
     * Getter
     * 
     * @return
     */
    public String getInout() {
        return inout;
    }

    /**
     * Getter
     * 
     * @return
     */
    public Integer getLengthOfStay() {
        return lengthOfStay;
    }

    /**
     * Getter
     * 
     * @return
     */
    public String getLocation() {
        return location;
    }

    /**
     * Getter
     * 
     * @return
     */
    public String getLocationPath() {
        return locationPath;
    }

    /**
     * Getter
     * 
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Getter
     * 
     * @return
     */
    public Timestamp getStart() {
        return start;
    }

    /**
     * Gets the subject of the visit
     * 
     * @return a subject
     */
    public Subject getSubject() {
        return subject;
    }

    /**
     * Returns whether there is data assigned
     * 
     * @return
     */
    public boolean hasData() {
        for (Set<Data> data : this.data.values()) {
            if (!data.isEmpty()) { return true; }
        }
        return false;
    }

    /**
     * Returns whether there is data assigned
     * 
     * @param config
     * @return
     */
    public boolean hasData(ConfigurationInputFile config) {
        Set<Data> data = this.data.get(config);
        return data != null && !data.isEmpty();
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }

    /**
     * Returns whether this is an artificial visit
     * 
     * @return
     */
    public boolean isArtificial() {
        return this.id.isArtificial();
    }

    /**
     * Merges the data from the given visit into this visit
     * 
     * @param visit
     * @return
     */
    public void merge(Visit visit) {

        // Merge baseline info
        this.start = getMin(this.start, visit.start);
        this.end = getMax(this.end, visit.end);
        this.activeStatus = getNonNull(visit.activeStatus, this.activeStatus);
        this.inout = getNonNull(visit.inout, this.inout);
        this.location = getNonNull(visit.location, this.location);
        this.locationPath = getNonNull(visit.locationPath, this.locationPath);
        this.lengthOfStay = getNonNull(visit.lengthOfStay, this.lengthOfStay);
        this.blob = getNonNull(visit.blob, this.blob);

        // Add data
        for (Entry<ConfigurationInputFile, Set<Data>> entry : visit.data.entrySet()) {
            for (Data data : entry.getValue()) {
                this.addData(entry.getKey(), data);
            }
        }
    }

    /**
     * 
     * @param end
     */
    public void setEnd(Timestamp end) {
        this.end = end;
    }

    /**
     * 
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @param start
     */
    public void setStart(Timestamp start) {
        this.start = start;
    }

    /**
     * Sets the subject
     * 
     * @param subject
     */
    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    /**
     * Used by Spring Batch WriterItem
     */
    public String toString() {
        ConfigurationEncounter config = Model.get().getConfig().getEncounterConfiguration();

        StringBuilder string = new StringBuilder();
        String delimiter = ConfigurationFileFormat.getInstance().getDelimiterOutput();

        if (name != null && name.equals(NAME_ARTIFICIAL_VISIT)) { return null; }

        if (start != null) {
            // TODO: Should be written in a format that is guaranteed to be
            // understood by transmart-batch
            string.append(start.format(Model.get()
                                            .getConfig()
                                            .getEncounterConfiguration()
                                            .getDateFormatter()));
        }

        if (end != null) {
            string.append(delimiter);
            // TODO: Should be written in a format that is guaranteed to be
            // understood by transmart-batch
            string.append(end.format(Model.get()
                                          .getConfig()
                                          .getEncounterConfiguration()
                                          .getDateFormatter()));
        }

        if (config.activeStatus != null) {
            string.append(delimiter);
            string.append(activeStatus);
        }

        if (config.inout != null) {
            string.append(delimiter);
            string.append(inout);
        }

        if (config.location != null) {
            string.append(delimiter);
            string.append(location);
        }

        if (config.locationPath != null) {
            string.append(delimiter);
            string.append(locationPath);
        }

        if (config.lengthOfStay != null) {
            string.append(delimiter);
            if (lengthOfStay != null) string.append(String.valueOf(lengthOfStay));
        }

        if (config.blob != null) {
            string.append(delimiter);
            string.append(blob);
        }

        return string.toString();
    }

    /**
     * Helper
     * 
     * @param val1
     * @param val2
     * @return
     */
    private Timestamp getMax(Timestamp val1, Timestamp val2) {
        if (val1 == null) {
            return val2;
        } else if (val2 == null) {
            return val1;
        } else if (val1.isAfter(val2)) {
            return val1;
        } else {
            return val2;
        }
    }

    /**
     * Helper
     * 
     * @param val1
     * @param val2
     * @return
     */
    private Timestamp getMin(Timestamp val1, Timestamp val2) {
        if (val1 == null) {
            return val2;
        } else if (val2 == null) {
            return val1;
        } else if (val1.isBefore(val2)) {
            return val1;
        } else {
            return val2;
        }
    }

    /**
     * Helper
     * 
     * @param val1
     * @param val2
     * @return
     */
    private <T> T getNonNull(T val1, T val2) {
        return val1 != null ? val1 : val2;
    }

    /**
     * Merges this visit with another visit returning a new visit
     * 
     * @param visit
     * @return
     */
    void deduplicate(Visit visit) {
        this.name = visit.name;
        this.start = visit.start;
        this.end = visit.end;
        this.activeStatus = visit.activeStatus;
        this.inout = visit.inout;
        this.location = visit.location;
        this.locationPath = visit.locationPath;
        this.lengthOfStay = visit.lengthOfStay;
        this.blob = visit.blob;
    }
}
