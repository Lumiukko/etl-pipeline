/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.model;

import java.util.HashMap;
import java.util.Map;

import org.difuture.components.i2b2transmart.configuration.ConfigurationInputFile;

/**
 * Model class
 * 
 * @author Fabian Prasser
 */
public class Ontology {

    /** A map containing per observation name its ontology node. */
    private Map<ConfigurationInputFile, Map<String, OntologyNodeConcept>> ontologyNodesForOutputColumns = new HashMap<>();

    /**
     * A map contains per concept the concept node hierarchie. Required for
     * mapping file.
     */
    private Map<ConfigurationInputFile, Map<String, OntologyNodeConcept>> ontologiesForConcepts         = new HashMap<>();

    /**
     * A map containing entities and hierarchies that need to be serialized for
     * i2b2.
     */
    private Map<String, OntologyNodeConcept>                              ontologyFiles                 = new HashMap<>();

    /**
     * Gets the concepts and their hierarchie for a configuration input file
     * 
     * @param config
     *            - a ConfigurationInputFile object
     * @return a Map<String, NodeConcept> object - the concepts and their
     *         hiearchie for parameter config
     */
    public Map<String, OntologyNodeConcept>
           getOntologiesForConcepts(ConfigurationInputFile config) {
        return ontologiesForConcepts.get(config);
    }

    /**
     * Returns the ontologies that need to be serialized for i2b2
     * 
     * @return
     */
    public Map<String, OntologyNodeConcept> getOntologyFiles() {
        return ontologyFiles;
    }

    /**
     * Gets the concepts and their NodeConcept object for a configuration input
     * file
     * 
     * @param config
     *            - a ConfigurationInputFile object
     * @return a Map<String, NodeConcept> object - the concepts and their
     *         NodeConcept object for parameter config
     */
    public Map<String, OntologyNodeConcept>
           getOntologyNodesForOutputColumns(ConfigurationInputFile config) {
        return ontologyNodesForOutputColumns.get(config);
    }

    /**
     * Sets the concepts and their hierarchie for a configuration input file
     * 
     * @param config
     *            - a ConfigurationInputFile object
     * @param conceptNodeHierarchie
     *            - a Map<String, NodeConcept> object with the concepts and
     *            their hiearchie for parameter config
     */
    public void setOntologiesForConcepts(ConfigurationInputFile config,
                                         Map<String, OntologyNodeConcept> conceptNodeHierarchie) {
        this.ontologiesForConcepts.put(config, conceptNodeHierarchie);
    }

    /**
     * Sets the ontologies that need to be serialized for i2b2
     * 
     * @param ontologyFiles
     */
    public void setOntologyFiles(Map<String, OntologyNodeConcept> ontologyFiles) {
        this.ontologyFiles = ontologyFiles;
    }

    /**
     * Sets the concepts and their NodeConcept for a configuration input file
     * 
     * @param config
     *            - a ConfigurationInputFile object
     * @param conceptMap
     *            - a Map<String, NodeConcept> object with the concepts and
     *            their NodeConcept object for parameter config
     */
    public void setOntologyNodesForOutputColumns(ConfigurationInputFile config,
                                                 Map<String, OntologyNodeConcept> conceptMap) {
        this.ontologyNodesForOutputColumns.put(config, conceptMap);
    }
}
