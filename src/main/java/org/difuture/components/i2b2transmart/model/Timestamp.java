/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Internal class for timestamps
 * 
 * @author Fabian Prasser
 */
public class Timestamp {

    /**
     * Reliability of timestamps, worst first
     * 
     * @author Fabian Prasser
     */
    public static enum Reliability {
                                    NONE("0: None"),
                                    YEAR("1: Year"),
                                    MONTH("2: Month"),
                                    DAY("3: Day");

        /** Label */
        private final String label;

        /**
         * Create a new instance
         * 
         * @param label
         */
        private Reliability(String label) {
            this.label = label;
        }

        @Override
        public String toString() {
            return this.label;
        }
    }

    /** Internal marker */
    private static final String            INTERNAL        = "ExcelDate:";;

    /** Internal format string */
    private static final DateTimeFormatter INTERNAL_FORMAT = DateTimeFormatter.ISO_DATE_TIME.withLocale(Locale.GERMAN)
                                                                                            .withZone(ZoneId.of("GMT"));

    /**
     * Returns now. Marked as unreliable.
     * 
     * @return
     */
    public static Timestamp now() {
        Timestamp timestamp = new Timestamp(LocalDateTime.now());
        timestamp.reliability = Reliability.NONE;
        return timestamp;
    }

    /**
     * To internal date, used because we were not able to extract the correctly
     * formatted date from XLSX files using POI.
     * 
     * @param date
     * @return
     */
    public static String toInternalDate(Date date) {
        if (date == null) { return ""; }
        return INTERNAL + INTERNAL_FORMAT.format(date.toInstant());
    }

    /**
     * Returns the worst of both reliabilities
     * 
     * @param a
     * @param b
     * @return
     */
    public static Reliability worst(Reliability a, Reliability b) {
        int ordinal = Math.min(a.ordinal(), b.ordinal());
        return Reliability.values()[ordinal];
    }

    /**
     * Converts an internal date
     * 
     * @param string
     * @return
     */
    private static String fromInternalDate(String string) {
        if (string == null || string.length() == 0) {
            return "";
        } else {
            return string.substring(INTERNAL.length());
        }
    }

    /**
     * Is this an internal date
     * 
     * @param string
     * @return
     */
    private static boolean isInternalDate(String string) {
        if (string == null || string.length() == 0) { return false; }
        return string.startsWith(INTERNAL);
    }

    /** The actual timestamp */
    private LocalDateTime timestamp;

    /** Reliability of the timestamp */
    private Reliability   reliability;

    /**
     * Parses a timestamp with the given format
     * 
     * @param format
     * @param timestamp
     * @param start
     */
    public Timestamp(DateTimeFormatter format, String timestamp, boolean start) {

        // Catch internal dates
        if (isInternalDate(timestamp)) {
            timestamp = fromInternalDate(timestamp);
            format = INTERNAL_FORMAT;
        }

        try {
            if (timestamp == null) {
                this.timestamp = null;
                this.reliability = Reliability.NONE;
                return;
            } else {
                this.timestamp = LocalDateTime.parse(timestamp, format);
                this.reliability = Reliability.DAY; // TODO: Maybe different
                                                    // depending on format?
            }
        } catch (DateTimeParseException e) {
            try {
                this.timestamp = LocalDate.parse(timestamp, format).atStartOfDay();
                this.reliability = Reliability.DAY; // TODO: Maybe different
                                                    // depending on format?

            } catch (DateTimeParseException ex) {
                // TODO: Ugly hack to parse year granularity
                try {
                    String string = format.toString();
                    if (!string.contains("MonthOfYear") && !string.contains("DayOfMonth") &&
                        string.contains("YearOfEra")) {
                        LocalDate date = Year.of(Integer.parseInt(timestamp))
                                             .atMonth(start ? 1 : 12)
                                             .atDay(start ? 1 : 31);
                        this.timestamp = start ? date.atStartOfDay() : date.atTime(23, 59, 59);
                        this.reliability = Reliability.YEAR;
                    } else {
                        this.timestamp = null;
                        this.reliability = Reliability.NONE;
                    }
                } catch (NumberFormatException e1) {
                    this.timestamp = null;
                    this.reliability = Reliability.NONE;
                }
            }
        }
    }

    /**
     * Creates a timestamp
     * 
     * @param day
     * @param month
     * @param year
     * @param start
     * @return
     */
    public Timestamp(int day, int month, int year, boolean start) {

        // We hope for the best
        this.reliability = Reliability.DAY;

        // Fix invalid ranges
        if (month < 1) {
            month = 1;
            this.reliability = worst(this.reliability, Reliability.YEAR);
        }
        if (month > 12) {
            month = 12;
            this.reliability = worst(this.reliability, Reliability.YEAR);
        }

        if (day < 1) {
            day = 1;
            this.reliability = worst(this.reliability, Reliability.MONTH);
        }

        switch (month) {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            if (day > 31) {
                day = 31;
                this.reliability = worst(this.reliability, Reliability.MONTH);
            }
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            if (day > 30) {
                day = 30;
                this.reliability = worst(this.reliability, Reliability.MONTH);
            }
            break;
        case 2:
            if (new GregorianCalendar().isLeapYear(year)) {
                if (day > 29) {
                    day = 29;
                    this.reliability = worst(this.reliability, Reliability.MONTH);
                }
            } else {
                if (day > 28) {
                    day = 28;
                    this.reliability = worst(this.reliability, Reliability.MONTH);
                }
            }
            break;
        }

        // Store timestamp
        if (year >= 1900) {
            this.timestamp = start ? LocalDateTime.of(year, month, day, 0, 0, 0)
                    : LocalDateTime.of(year, month, day, 23, 59, 59);
        } else {
            this.timestamp = null;
            this.reliability = Reliability.NONE;
        }
    }

    /**
     * Constructs a timestamp
     * 
     * @param timestamp
     */
    public Timestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
        this.reliability = Reliability.DAY;
    }

    /**
     * Compares this timestamp to another timestamp
     * 
     * @param val
     * @return
     */
    public int compareTo(Timestamp val) {
        if (!this.isValid() && val.isValid()) {
            return -1;
        } else if (!this.isValid() && !val.isValid()) {
            return 0;
        } else if (this.isValid() && !val.isValid()) {
            return +1;
        } else {
            return this.getTimestamp().compareTo(val.getTimestamp());
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Timestamp other = (Timestamp) obj;
        if (timestamp == null) {
            if (other.timestamp != null) return false;
        } else if (!timestamp.equals(other.timestamp)) return false;
        return true;
    }

    /**
     * Formats the timestamp according to the given formatter
     * 
     * @param formatter
     * @return
     */
    public String format(DateTimeFormatter formatter) {
        if (this.timestamp == null) {
            return null;
        } else {
            return this.timestamp.format(formatter);
        }
    }

    /**
     * Returns the reliability of this timestamp
     * 
     * @return
     */
    public Reliability getReliability() {
        return reliability;
    }

    /**
     * Returns the actual timestamp
     * 
     * @return
     */
    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    @Override
    public int hashCode() {
        return ((timestamp == null) ? 0 : timestamp.hashCode());
    }

    /**
     * Is after
     * 
     * @param val
     * @return
     */
    public boolean isAfter(Timestamp val) {
        if (!this.isValid() && val.isValid()) {
            return false;
        } else if (!this.isValid() && !val.isValid()) {
            return false;
        } else if (this.isValid() && !val.isValid()) {
            return true;
        } else {
            return this.getTimestamp().isAfter(val.getTimestamp());
        }
    }

    /**
     * Returns true, if this date is more reliable, or if this date is at least
     * equally reliable and after the given date.
     * 
     * @param other
     * @return
     */
    public boolean isAfterOrMoreReliable(Timestamp other) {
        if (this.timestamp == null) { return false; }
        if (this.timestamp != null && other.timestamp == null) { return true; }
        if (this.reliability.compareTo(other.reliability) > 0) {
            return true;
        } else if (this.reliability.compareTo(other.reliability) == 0) {
            return this.getTimestamp().isAfter(other.getTimestamp());
        } else {
            return false;
        }
    }

    /**
     * Is before
     * 
     * @param val
     * @return
     */
    public boolean isBefore(Timestamp val) {
        if (!this.isValid() && val.isValid()) {
            return true;
        } else if (!this.isValid() && !val.isValid()) {
            return false;
        } else if (this.isValid() && !val.isValid()) {
            return false;
        } else {
            return this.getTimestamp().isBefore(val.getTimestamp());
        }
    }

    /**
     * Returns true, if this date is more reliable, or if this date is at least
     * equally reliable and before the given date.
     * 
     * @param other
     * @return
     */
    public boolean isBeforeOrMoreReliable(Timestamp other) {
        if (this.timestamp == null) { return false; }
        if (this.timestamp != null && other.timestamp == null) { return true; }
        if (this.reliability.compareTo(other.reliability) > 0) {
            return true;
        } else if (this.reliability.compareTo(other.reliability) == 0) {
            return this.getTimestamp().isBefore(other.getTimestamp());
        } else {
            return false;
        }
    }

    /**
     * Returns whether this is a valid timestamp
     * 
     * @return
     */
    public boolean isValid() {
        return timestamp != null;
    }

    /**
     * Returns a local date
     * 
     * @return
     */
    public LocalDate toLocalDate() {
        if (this.timestamp == null) {
            return null;
        } else {
            return this.timestamp.toLocalDate();
        }
    }
}
