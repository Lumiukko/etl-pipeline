/*
 * ETL library
 * Copyright (C) 2019 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.difuture.components.i2b2transmart.model;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.difuture.components.i2b2transmart.configuration.ConfigurationInputFile;
import org.difuture.components.i2b2transmart.springbatch.tasklet.TaskletRemoveDuplicates;

/**
 * Entity meta data
 * 
 * @author Fabian Prasser
 */
public class Entity {

    /** Associated config file */
    private ConfigurationInputFile    config;

    /**
     * Returns the names of columns in the associated staging file for this
     * concept. First returns columns for the concept (in case of categorical
     * data these are combinations of attribute and values) followed by columns
     * for the modifiers. This order is important.
     */
    private Map<String, List<String>> conceptToOutputColumnNames;

    /** Attributes (concepts and modifiers) */
    private Set<String>               attributes;
    /** Columns to read from file for this entity */
    private Set<String>               columnsToRead;
    /** Concepts */
    private Set<String>               concepts;
    /** Modifiers */
    private Set<String>               modifiers;
    /** Data types for attributes */
    private Map<String, String>       attributeToDataType;

    /**
     * Creates a new instance
     * 
     * @param attributes
     * @param columnsToRead
     * @param concepts
     * @param modifiers
     * @param config
     */
    public Entity(Set<String> attributes,
                  Set<String> columnsToRead,
                  HashSet<String> concepts,
                  HashSet<String> modifiers,
                  ConfigurationInputFile config) {
        this.attributes = Collections.unmodifiableSet(attributes);
        this.columnsToRead = Collections.unmodifiableSet(columnsToRead);
        this.concepts = Collections.unmodifiableSet(concepts);
        this.modifiers = Collections.unmodifiableSet(modifiers);
        this.attributeToDataType = new HashMap<>();
        this.config = config;
    }

    /**
     * Returns the attributes
     * 
     * @return
     */
    public Set<String> getAttributes() {
        return attributes;
    }

    /**
     * Returns the columns to read
     * 
     * @return
     */
    public Set<String> getColumnsToRead() {
        return columnsToRead;
    }

    /**
     * Returns the concepts
     * 
     * @return
     */
    public Set<String> getConcepts() {
        return concepts;
    }

    /**
     * Returns the data type
     * 
     * @param attribute
     * @return
     */
    public String getDataType(String attribute) {
        return attributeToDataType.get(attribute);
    }

    /**
     * Returns the modifiers
     * 
     * @return
     */
    public Set<String> getModifiers() {
        return modifiers;
    }

    /**
     * For a given entity name, this returns the list of column names in the
     * staging file
     * 
     * @param config
     * @return
     */
    public Map<String, List<String>> getOutputColumnNames() {
        return conceptToOutputColumnNames;
    }

    /**
     * Returns the summary concepts
     * 
     * @return
     */
    public Set<String> getSummaryConcepts() {

        // Find
        Set<String> result = new HashSet<>();
        for (String concept : getConcepts()) {

            // TODO: Avoid side-effects
            if (config.noSummary) {
                result.add(concept);
            } else if (this.isAttributeDataTypeNumber(concept)) {
                result.add(concept + TaskletRemoveDuplicates.LATEST);
                result.add(concept + TaskletRemoveDuplicates.OLDEST);
                result.add(concept + TaskletRemoveDuplicates.MINIMUM);
                result.add(concept + TaskletRemoveDuplicates.MAXIMUM);
                result.add(concept + TaskletRemoveDuplicates.MEAN);
                this.attributeToDataType.put(concept + TaskletRemoveDuplicates.MINIMUM, "number");
                this.attributeToDataType.put(concept + TaskletRemoveDuplicates.MAXIMUM, "number");
                this.attributeToDataType.put(concept + TaskletRemoveDuplicates.MEAN, "number");
                this.attributeToDataType.put(concept + TaskletRemoveDuplicates.LATEST, "number");
                this.attributeToDataType.put(concept + TaskletRemoveDuplicates.OLDEST, "number");
            } else {
                result.add(concept + TaskletRemoveDuplicates.LATEST);
                result.add(concept + TaskletRemoveDuplicates.OLDEST);
                result.add(concept + TaskletRemoveDuplicates.SET);
                this.attributeToDataType.put(concept + TaskletRemoveDuplicates.LATEST, "text");
                this.attributeToDataType.put(concept + TaskletRemoveDuplicates.OLDEST, "text");
                this.attributeToDataType.put(concept + TaskletRemoveDuplicates.SET, "text");
            }
        }

        // Done
        return result;
    }

    /**
     * Returns whether this is a number
     * 
     * @param attribute
     * @return
     */
    public boolean isAttributeDataTypeNumber(String attribute) {
        // If there is no data, there is no data type
        if (attributeToDataType.get(attribute) == null) { return false; }
        // Return
        return attributeToDataType.get(attribute).equals("number");
    }

    /**
     * Sets the data types
     * 
     * @param attributeToDataType
     */
    public void setDataTypes(Map<String, String> attributeToDataType) {
        this.attributeToDataType = attributeToDataType;
    }

    /**
     * This sets the list of column names in the staging file for each
     * config/entity name
     * 
     * @param conceptToOutputColumnNames
     */
    public void setOutputColumnNames(Map<String, List<String>> conceptToOutputColumnNames) {
        this.conceptToOutputColumnNames = conceptToOutputColumnNames;
    }
}
